Gauche, boehm-gcが必要

ビルド

```
#!bash


$ cmake <path-to-project>
$ make
$ make install
```


実行方法

その1 Gaucheを使ってRheinを解釈
```
#!bash


$ rhein <source-file>
```


その2 Rheinを使ってRheinを解釈

```
#!bash

$ rhein compile.rh <source-file> <bytecode-file>
$ rhein -b <bytecode-file>
```


サンプル(/demo)

* fib.rh : ループでフィボナッチ

* fibrec.rh : フィボナッチ

* ski.rh : SKIコンビネータの実装

* peg.rh : PEGライブラリのサンプル

* compile.rh : Rheinで書かれたRheinバイトコードコンパイラ