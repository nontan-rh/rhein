//
// array.cc
//

#include <stdexcept>

#include "object.h"
#include "allocator.h"
#include "vm.h"

namespace rhein {

Array*
Array::create(Int size) {
    if (size < 0) {
        throw std::runtime_error("size < 0");
    }
    void* p = get_current_state()->allocate_object<Array>();
    return new (p) Array(size);
}

Array::Array(Int size) : Object(BuiltinClassID::Array), size_(size) {
    // Round up size to power of 2
    Int s = 1;
    while (size > s) { s *= 2; }

    allocated_size_ = s;

    body_ = get_current_state()->allocate_raw_array(allocated_size_);
}

void
Array::append(Value value) {
    ++size_;
    if (size_ > allocated_size_) { // Allocated buffer is too short
        unsigned newallocated_size = allocated_size_ * 2;
        Value* newbody = get_current_state()->allocate_raw_array(newallocated_size);
        for (Int i = 0; i < allocated_size_; i++) {
            newbody[i] = body_[i];
        }
        allocated_size_ = newallocated_size;
        body_ = newbody;
    }
    body_[size_ - 1] = value;
}

void
Array::insert(Int index, Value value) {
    if (index < 0 || size_ < index) {
        throw std::runtime_error("invalid index");
    }

    if (size_ + 1 > allocated_size_) {
        unsigned newallocated_size = allocated_size_ * 2;
        Value* newbody = get_current_state()->allocate_raw_array(newallocated_size);

        for (Int i = 0; i < index; i++) {
            newbody[i] = body_[i];
        }

        newbody[index] = value;

        for (Int i = index; i < size_; i++) {
            newbody[i + 1] = body_[i];
        }

        allocated_size_ = newallocated_size;
        body_ = newbody;
    } else {
        for (Int i = size_; i > index; i--) {
            body_[i] = body_[i - 1];
        }

        body_[index] = value;
    }

    ++size_;
}

void
Array::drop(Int length) {
    if (length < 0 || size_ < length) {
        throw std::runtime_error("invalid length");
    }
    size_ = length;
}

bool
Array::to_string(String*& dest) const {
    State* R = get_current_state();
    char* buf = R->allocate_block<char>(size_);

    for (Int i = 0; i < size_; i++) {
        if (!body_[i].is(Value::Type::Char)) {
            R->release_block(buf);
            return false;
        }

        buf[i] = body_[i].get_char();
    }

    dest = String::create(buf, size_);
    R->release_block(buf);
    return true;
}

RestArguments*
RestArguments::create(Int size) {
    if (size < 0) {
        return nullptr;
    }
    void* p = get_current_state()->allocate_object<RestArguments>();
    return new (p) RestArguments(size);
}

RestArguments::RestArguments(Int size)
    : Object(BuiltinClassID::RestArguments), size_(size) {

    body_ = get_current_state()->allocate_raw_array(size);
}

bool
RestArguments::to_array(Array*& dest) const {
    dest = Array::create(size_);

    for (Int i = 0; i < size_; i++) {
        dest->elt_set(i, body_[i]);
    }

    return true;
}

}

