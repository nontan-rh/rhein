//
// object.cc
//

#include "object.h"
#include "vm.h"

namespace rhein {

String*
Value::get_string_representation() const {
  char buf[100];
    switch (type_id_) {
      case Type::Nil:
        return String::create("nil");
      case Type::Bool:
        return String::create(u_.v_bool_ ? "true" : "false");
      case Type::Undef:
        return String::create("undef");
      case Type::Int:
        sprintf(buf, "%d", u_.v_int_);
        return String::create(buf);
      case Type::Char:
        sprintf(buf, "%c", u_.v_char_);
        return String::create(buf);
      case Type::Object:
        return u_.v_obj_->get_string_representation();
      case Type::Record:
        return String::create("#<record>");
      case Type::Class:
        return String::create("#<class>");
    }
}

String*
Object::get_string_representation() {
    return String::create("#<obj>");
}

ClassID
get_class_of(Value v) {
    switch (v.get_type()) {
    case Value::Type::Nil:
        return BuiltinClassID::Nil;
    case Value::Type::Bool:
        return BuiltinClassID::Bool;
    case Value::Type::Int:
        return BuiltinClassID::Int;
    case Value::Type::Undef:
        return BuiltinClassID::Any;
    case Value::Type::Char:
        return BuiltinClassID::Char;
    case Value::Type::Class:
        return BuiltinClassID::Class;
    case Value::Type::Object:
        return v.get_obj<Object>()->get_class_id();
    case Value::Type::Record:
        return v.get_rec()->get_class_id();
    }
    return BuiltinClassID::InvalidClassID;
}

ClassID
get_class_of(const Object* obj) {
    return obj->get_class_id();
}

ClassID
get_class_of(const Record* rec) {
    return rec->get_class_id();
}

ClassID
get_parent(ClassID c) {
    ClassID p;
    get_current_state()->get_class_parent(c, p);
    return p;
}

bool
is_subclass_of(ClassID k, ClassID p) {
    for (; k != BuiltinClassID::InvalidClassID; k = get_parent(k)) {
        if (k == p) { return true; }
    }
    return false;
}

ClassInfo*
ClassInfo::create(ClassID parent, Symbol* name, RecordInfo* record_info) {
    void *p = get_current_state()->allocate_struct<ClassInfo>();
    return new (p) ClassInfo(parent, name, record_info);
}

}
