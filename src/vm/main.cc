//
// main.cc
//

#include <cstdio>
#include <gc.h>

#include "vm.h"
#include "loader.h"
#include "basic/basic.h"
#include "basic/builtin.h"
#include "basic/port.h"
#include "basic/mtrandom.h"
#include "basic/optparser.h"
#include "parser/peg.h"
#include "bytecode_module.h"

using namespace rhein;
using namespace rhein::basic;
using namespace rhein::builtin;
using namespace rhein::port;
using namespace rhein::mtrandom;
using namespace rhein::optparser;
using namespace rhein::peg;

int main(int argc, const char** argv) {
    GC_init();

    State* R = new (GC_malloc(sizeof(State))) State();
    volatile State* Rv = R;
    SwitchState ss(R);
    R->load_module(BasicModule::create());
    R->load_module(BuiltinModule::create());
    R->load_module(FileModule::create());
    R->load_module(PegModule::create());
    R->load_module(MTModule::create());
    R->load_module(BytecodeModule::create());

    OptParser* op = OptParser::create();
    op->parse(argc, argv);

    const char** args;
    int arg_count;
    args = op->get_args_c(arg_count);
    if (arg_count < 2) {
      fprintf(stderr, "Usage: rhein <bytecode>\n");
      return 1;
    }

    if (op->get_flag('b')) {
      load_precompiled_script(args[1]);
    } else {
      load_script(args[1]);
    }

    int script_index;
    int non_flag_cnt = 0;
    for (script_index = 0; script_index < argc; script_index++) {
      if (std::strlen(argv[script_index]) > 0 && argv[script_index][0] != '-') {
        ++non_flag_cnt;
        if (non_flag_cnt == 2) {
          break;
        }
      }
    }

    Array* script_args = Array::create(argc - script_index - 1);
    for (int i = script_index + 1; i < argc; i++) {
      script_args->elt_set(i - (script_index + 1), Value::by_object(String::create(argv[i])));
    }
    Value v[1];
    v[0] = Value::by_object(script_args);

    execute(R->get_symbol("main"), 1, v);

    Rv = nullptr;
    return 0;
}
