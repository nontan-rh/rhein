/*
 * systable.h - Hash table for system
 *
 * SysTable is a hash table implementation only supporting integer and
 * pointer. This class cannot be accessed from Rhein code.
 *
 * The algorithm is the open-addressing method with rehashing. This table
 * resolves a hash collision with linear probing.
 */

#ifndef SYSTABLE_H_INCLUDED
#define SYSTABLE_H_INCLUDED
#include <stdexcept>

#include <cassert>

#include "vm.h"
#include "internal.h"
#include "object.h"

namespace rhein {

// the type of hash values is unsigned

class KeyNotFoundError : public std::runtime_error
{
public:
    explicit KeyNotFoundError() : runtime_error("KeyNotFoundError") {}
};

static inline unsigned
get_sys_hash(const void* p) {
    return static_cast<unsigned>(reinterpret_cast<uintptr_t>(p));
}

static inline unsigned
get_sys_hash(unsigned i) {
    return i;
}

static inline unsigned
get_sys_hash(int i) {
    return static_cast<unsigned>(i);
}

template <typename K, typename V>
class SysTable : public PlacementNewObj {
public:
    static SysTable* create() {
        return create(kDefaultInitialSize);
    }

    static SysTable* create(unsigned initial_size) {
        return new (get_current_state()->allocate_struct<SysTable<K,V>>()) SysTable(initial_size);
    }

    unsigned get_num_entries() const { return num_entries_; }

    // Checking whether there is the entry tied with the key
    bool exists(const K& key) const {
        unsigned hash_value = get_sys_hash(key);
        unsigned index_begin = hash_value % table_size_;

        if (table_[index_begin].status == EntryStatus::Empty) { return false; }

        unsigned i = index_begin;
        do {
            if (entry_is(i, key)) { return true; }

            // linear probing
            i = (i + 1) % table_size_;
        } while (i != index_begin && table_[i].status != EntryStatus::Empty);

        return false;
    }

    // Find the key and get its value.
    // When the key is not found, an exception will be thrown.
    V find(const K& key) const {
        unsigned hash_value = get_sys_hash(key);
        unsigned index_begin = hash_value % table_size_;

        if (table_[index_begin].status == EntryStatus::Empty) {
            throw KeyNotFoundError();
        }

        unsigned i = index_begin;
        do {
            if (entry_is(i, key)) { return table_[i].value; }

            // linear probing
            i = (i + 1) % table_size_;
        } while (i != index_begin && table_[i].status != EntryStatus::Empty);

        throw KeyNotFoundError();
    }

    // Tie the key and the value anyway. This method may occur rehashing.
    void insert(const K& key, const V& value) {
        unsigned hash_value = get_sys_hash(key);
        unsigned index_begin = hash_value % table_size_;

        unsigned i = index_begin;
        do {
            if (table_[i].status == EntryStatus::Empty) {
                set_entry(i, key, value);
                ++table_used_;
                ++num_entries_;

                // Maintain the ratio of empty and used slots.
                rebuild_if_required();
                return;
            } else if (entry_is(i, key)) {
                table_[i].value = value;
                return;
            }

            // linear probing
            i = (i + 1) % table_size_;
        } while (i != index_begin);

        // NOTREACHED
        // Because the ratio of empty and used slots is maintained, there is
        // always an empty slot in table. So, the exit condition of the loop
        // (i == index_begin) will never be met.
        throw std::logic_error("");
    }

    // If there is no entry of the key, do insert
    void insert_if_absent(const K& key, const V& value) {
        if (exists(key)) { return; }
        else { insert(key, value); }
    }

    // If there is the entry of the key, change its tied value
    void assign(const K& key, const V& value) {
        if (!exists(key)) { return; }
        else { insert(key, value); }
    }

    // Remove entry. If there is not the entry tied with the key, an exception
    // will be thrown.
    void remove(const K& key) {
        unsigned hash_value = get_sys_hash(key);
        unsigned index_begin = hash_value % table_size_;

        if (table_[index_begin].status == EntryStatus::Empty) {
            throw KeyNotFoundError();
        }

        unsigned i = index_begin;
        do {
            if (entry_is(i, key)) {
                table_[index_begin].status = EntryStatus::Deleted;
                --num_entries_;
                return;
            }

            // Linear probing
            i = (i + 1) % table_size_;
        } while (i != index_begin && table_[i].status == EntryStatus::Empty);

        throw KeyNotFoundError();
    }

    // Copy all entries from other SysTable.
    void import(const SysTable<K,V>* other) {
        for (unsigned i = 0; i < other->table_size_; i++) {
            if (other->table_[i].status == EntryStatus::Exist) {
                insert(other->table_[i].key, other->table_[i].value);
            }
        }
    }

    template <typename F>
    void for_each(F f) {
        for (int i = 0; i < table_size_; i++) {
            if (table_[i].status == EntryStatus::Exist) {
                f(table_[i].value);
            }
        }
    }

private:
    // 15 is the fourth Mersenne number (NOT always prime number).
    static const unsigned kDefaultInitialSize = 15;

    // Rebuild ratio is 4/5 = 0.8
    // When table_used_ >= table_size_ * (Rebuild ratio), do rehash.
    const unsigned kRebuildRatioNum = 4;
    const unsigned kRebuildRatioDiv = 5;

    SysTable(unsigned initial_size)
        : table_size_(initial_size), table_used_(0) {

        table_ = get_current_state()->allocate_block<SysTableEntry>(initial_size);
        for (unsigned i = 0; i < table_size_; i++) {
            table_[i].status = EntryStatus::Empty;
        }
    }

    // Check if there is an entry of the key.
    bool entry_is(unsigned index, const K& key) const {
        return (table_[index].status == EntryStatus::Exist
                && table_[index].key == key);
    }

    // Change the contains of entry.
    void set_entry(unsigned index, const K& key, const V& value) {
        table_[index].status = EntryStatus::Exist;
        table_[index].key = key;
        table_[index].value = value;
    }

    void rebuild_if_required() {
        // table_used_ / table_size_ > (rebuild ratio)
        if (table_used_ * kRebuildRatioDiv > table_size_ * kRebuildRatioNum) {
            rebuild();
        }
    }

    void rebuild() {
        // Recurrence relation of Mersenne numbers
        // M(n+1) = M(n) * 2 + 1
        unsigned newtable_size = table_size_ * 2 + 1;

        // Initialize new table
        SysTableEntry* newtable = get_current_state()->allocate_block<SysTableEntry>(newtable_size);
        for (unsigned i = 0; i < newtable_size; i++) {
            newtable[i].status = EntryStatus::Empty;
        }

        // i : an index of (old) table
        // j : an index of new table
        for (unsigned i = 0; i < table_size_; i++) {
            if (table_[i].status == EntryStatus::Exist) {
                unsigned hash_value = get_sys_hash(table_[i].key);
                unsigned newindex = hash_value % newtable_size;

                unsigned j = newindex;

                while (true) {
                    if (newtable[j].status == EntryStatus::Empty) {
                        newtable[j].status = EntryStatus::Exist;
                        newtable[j].key = table_[i].key;
                        newtable[j].value = table_[i].value;
                        break;
                    }

                    j = (j + 1) % newtable_size;
                    assert(newindex != j); // There are always empty slots.
                }
            }
        }

        table_used_ = num_entries_;
        table_size_ = newtable_size;
        table_ = newtable;
    }

    enum class EntryStatus {
        Empty,
        Deleted,
        Exist,
    };

    struct SysTableEntry {
        K key;
        V value;
        EntryStatus status;
    };

    unsigned num_entries_;
    unsigned table_size_;
    unsigned table_used_;
    SysTableEntry* table_;
};

} // End of namespace

#endif // For include guard
