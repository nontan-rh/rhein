//
// record.cc
//

#include <cstring>
#include <cassert>
#include <cstdio>

#include "systable.h"
#include "object.h"
#include "vm.h"

namespace rhein {

RecordInfo::RecordInfo(RecordInfo* parent, unsigned slot_num,
    Symbol** slot_ids) {

    num_slots_ = slot_num;

    if (!(parent == nullptr || parent->id_index_table_ == nullptr)) {
        num_slots_ += parent->id_index_table_->get_num_entries();

        id_index_table_ = SysTable<const Symbol*, unsigned>::create();
        id_index_table_->import(parent->id_index_table_);
    }

    if (slot_num == 0) { return; }

    if (id_index_table_ == nullptr) {
        id_index_table_ = SysTable<const Symbol*, unsigned>::create();
    }

    unsigned base = id_index_table_->get_num_entries();
    for (unsigned i = 0; i < slot_num; i++) {
        id_index_table_->insert_if_absent(slot_ids[i], base + i);
    }
}

RecordInfo*
RecordInfo::create(RecordInfo* parent, unsigned slot_num,
    Symbol** slot_ids) {

    void* p = get_current_state()->allocate_struct<RecordInfo>();
    return new (p) RecordInfo(parent, slot_num, slot_ids);
}

bool
RecordInfo::get_slot_index(Symbol* slot_id, unsigned& index) const {
    if (!id_index_table_->exists(slot_id)) { return false; }
    index = id_index_table_->find(slot_id);
    return true;
}

Record::Record(ClassID klass_id)
    : klass_id_(klass_id),
      inherit_instance_(Value::k_undef()) {
    State* R = get_current_state();
    if (!R->get_class_record_info(klass_id, record_info_)) {
        std::printf("the class has no record info\n");
        exit(1);
    }
    member_slots_ = R->allocate_raw_array(record_info_->num_slots());
}

Record*
Record::create(ClassID klass_id) {
    void* p = get_current_state()->allocate_object<Record>();
    return new (p) Record(klass_id);
}

bool
Record::slot_ref(Symbol* slot_id, Value& value) {
    unsigned index;
    if (!record_info_->get_slot_index(slot_id, index)) {
        return false;
    }

    value = member_slots_[index];
    return true;
}

bool
Record::slot_set(Symbol* slot_id, Value value) {
    unsigned index;
    if (!record_info_->get_slot_index(slot_id, index)) {
        return false;
    }

    member_slots_[index] = value;
    return true;
}

static void
print_value(Value v) {
    if (v.is(Value::Type::Int)) {
        printf("%d", v.get_int());
    } else if (v.is(Value::Type::Char)) {
        printf("?%c(%d)", v.get_char(), v.get_char());
    } else if (v.is(Value::Type::Bool)) {
        if (v.get_bool()) {
            printf("true");
        } else {
            printf("false");
        }
    } else if (v.is(Value::Type::Nil)) {
        printf("nil");
    } else if (v.is(Value::Type::Object)) {
        printf("#o<%p>", v.get_obj<Object>());
    } else if (v.is(Value::Type::Record)) {
        Record* rec = v.get_rec();
        Symbol* name;
        get_current_state()->get_class_name(get_class_of(rec), name);
        const char* cstr;
        size_t len;
        name->get_cstr(cstr, len);
        printf("#r<");
        for (unsigned i = 0; i < len; i++) {
            printf("%c", cstr[i]);
        }
        printf(">");
    } else if (v.is(Value::Type::Undef)) {
        printf("Undef");
    } else {
        throw 0;
    }
}

void
Record::dump() {
    printf("ClassID : %d\n", klass_id_);

    for (int i = 0; i < record_info_->num_slots(); i++) {
        printf("slot %02d : ", i);
        print_value(member_slots_[i]);
        printf("\n");
    }
}

}

