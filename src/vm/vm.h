//
// vm.h
//

#ifndef VM_H
#define VM_H

#include <cstdio>

#include <initializer_list>

#include "allocator.h"
#include "object.h"

namespace rhein {

class ObjectSigniture {
public:
    enum {
        Class,
        Function,
    };
};

class LiteralSigniture {
public:
    enum {
        Int,
        Char,
        Symbol,
        String,
    };
};

typedef bool (*UnaryOperator)(Value, Value&);
typedef bool (*BinaryOperator)(Value, Value, Value&);

class Module;

class State {
public:
    static void* operator new(size_t, void* p) { return p; }

    Symbol* get_symbol(const char* cstr) const {
        return symbol_provider_->get_symbol(cstr);
    }

    Symbol* get_symbol(const char* cstr, size_t len) const {
        return symbol_provider_->get_symbol(cstr, len);
    }

    template <class T>
    void* allocate_object() {
        return allocator_->allocate_object<T>();
    }

    Value* allocate_raw_array(unsigned size) {
        return allocator_->allocate_raw_array(size);
    }

    template <class T>
    T* allocate_block(unsigned size) {
        return allocator_->allocate_block<T>(size);
    }

    void release_block(void* p) {
        allocator_->release_block(p);
    }

    template <class T>
    void* allocate_struct() {
        return allocator_->allocate_struct<T>();
    }

    void release_struct(void* p) {
        return allocator_->release_struct(p);
    }

    State();
    ~State();

    // Installation
    bool add_function(Symbol* name, Function* func);
    bool add_variable(Symbol* id, Value val);
    ClassID add_class(Symbol* name, ClassID parent, RecordInfo* info);

    bool get_class(Symbol* id, ClassID& klass) const;
    bool get_class_name(ClassID klass, Symbol*& name) const;
    bool get_class_parent(ClassID klass, ClassID& parent) const;
    bool get_class_record_info(ClassID klass, RecordInfo*& record_info) const;

    // Bytecode level interface
    bool global_func_ref(Symbol* id, Value& func) const;
    bool global_var_ref(Symbol* id, Value& value) const;
    bool global_var_set(Symbol* id, Value value);

    // File loading interface
    bool load_file(FILE* fp);

    // Module loader interface
    bool load_module(Module* module);

    // Stack info
    Value* get_stack_begin() const { return stack_begin_; }
    Value* get_stack_end() const { return stack_end_; }
    Value*& get_stack_top() { return stack_top_; }

    // Operator
    UnaryOperator get_unary_operator(int no) const;
    BinaryOperator get_binary_operator(int no) const;

    // For debugging
    void dump_functions();
    void dump_classes();
    void dump_variables();

private:
    SysTable<const Symbol*, Object*>* func_slots_;
    SysTable<const Symbol*, Value>* var_slots_;
    SysTable<const Symbol*, ClassID>* klass_slots_;

    SysArray<ClassInfo*>* klass_info_table_;

    SysArray<UnaryOperator>* unary_operator_table_;
    SysArray<BinaryOperator>* binary_operator_table_;

    SymbolProvider* symbol_provider_;

    Allocator* allocator_;

    const unsigned kStackSize = 1 << 27;
    Value* stack_begin_;
    Value* stack_end_;
    Value* stack_top_;

    bool read_object(FILE* fp);
    bool read_function(FILE* fp);
    bool read_class(FILE* fp);

    void initialize_class();
    void initialize_symbol();
    void initialize_operator();
};

extern State* current_state_;

inline State* get_current_state() {
    return current_state_;
}

class Module {
protected:
    virtual ~Module() = default;

public:
    virtual bool initialize() = 0;
};

class Closure : public PlacementNewObj {
public:
    static Closure* create(Closure* parent,
            unsigned arg_count, Value* args, Value* func_slots, Value* var_slots) {
        return new (get_current_state()->allocate_struct<Closure>()) Closure(parent,
                arg_count, args, func_slots, var_slots);
    }

    Closure* get_parent() const { return parent_; }
    unsigned get_arg_count() const { return arg_count_; }
    Value* get_args() const { return args_; }
    Value* get_func_slots() const { return func_slots_; }
    Value* get_var_slots() const { return var_slots_; }

    void copy_slots(BytecodeFunction* fn);

private:
    Closure(Closure* parent, unsigned arg_count, Value* args, Value* func_slots,
            Value* var_slots)
        : parent_(parent), arg_count_(arg_count), args_(args), func_slots_(func_slots),
          var_slots_(var_slots) { }

    Closure* parent_;
    unsigned arg_count_;
    Value* args_;
    Value* func_slots_;
    Value* var_slots_;
};

// Frame must be a POD
struct Frame : public PlacementNewObj {
    Frame(Value* stack_ptr, BytecodeFunction* fn_, Frame* parent_,
            Closure* closure_, unsigned argc_, Value* args_,
            Value*& next_stack_ptr);

    BytecodeFunction* fn;
    Frame* parent;
    Value* stack;
    Closure* closure;
    bool should_save_closure;
    const uint32_t* pc;
    Value* restore_stack_ptr;
    Value* sp;
    Value slots[1];

    static Frame* create(Value* stack_ptr, BytecodeFunction* fn,
            Frame* parent, Closure* closure, unsigned argc, Value* args,
            Value*& next_stack_ptr) {
        return new (stack_ptr) Frame(stack_ptr, fn, parent,
                closure, argc, args, next_stack_ptr);
    }

    // Variables
    bool local_func_ref(unsigned depth, unsigned offset, Value& value);
    bool local_func_set(unsigned depth, unsigned offset, Value value);
    bool local_var_ref(unsigned depth, unsigned offset, Value& value);
    bool local_var_set(unsigned depth, unsigned offset, Value value);
    bool local_arg_ref(unsigned depth, unsigned offset, Value& value);
    bool local_arg_set(unsigned depth, unsigned offset, Value value);
};

Value execute(Symbol* entry_point, unsigned argc, Value* args);
Value execute(Value fn, unsigned argc, Value* args);
Value execute(BytecodeFunction* bfn, unsigned argc, Value* args);

class SwitchState {
public:
    SwitchState(State* R) {
        old_state_ = current_state_;
        current_state_ = R;
    }

    ~SwitchState() {
        current_state_ = old_state_;
    }

private:
    void* operator new(size_t /* size */) { return nullptr; }
    State* old_state_;
};

bool add_native_function(const char* name,
                         bool variadic,
                         std::initializer_list<const char*> arg_class_names,
                         NativeFunctionBody body);

ClassID add_class(const char* name, const char* parent_name);
bool add_variable(const char* name, Value v);

}

#endif // VM_H
