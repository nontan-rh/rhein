// // vm.cc
//

#include <iostream>
#include <cstring>
#include <cstdint>
#include <type_traits>
#include <initializer_list>

using namespace std;

#include "instruction.h"
#include "systable.h"
#include "sysarray.h"
#include "object.h"
#include "error.h"
#include "operate.h"
#include "vm.h"

namespace rhein {

State* current_state_;

void
Closure::copy_slots(BytecodeFunction* fn) {
    State* R = get_current_state();
    // Arguments
    Value* new_args = R->allocate_raw_array(arg_count_);
    for (unsigned i = 0; i < arg_count_; i++) { new_args[i] = args_[i]; }

    // Function slots
    Value* new_func_slots = R->allocate_raw_array(fn->get_function_slot_num());
    for (unsigned i = 0; i < fn->get_function_slot_num(); i++) {
        new_func_slots[i] = func_slots_[i];
    }

    // Variable slots
    Value* new_var_slots = R->allocate_raw_array(fn->get_variable_slot_num());
    for (unsigned i = 0; i < fn->get_variable_slot_num(); i++) {
        new_var_slots[i] = var_slots_[i];
    }

    args_ = new_args;
    func_slots_ = new_func_slots;
    var_slots_ = new_var_slots;
}

Frame::Frame(Value* stack_ptr, BytecodeFunction* fn_, Frame* parent_,
        Closure* closure_, unsigned argc_, Value* args_, Value*& next_stack_ptr)
    : fn(fn_), parent(parent_), should_save_closure(false),
      pc(nullptr), restore_stack_ptr(stack_ptr), sp(nullptr) {
    State* R = get_current_state();
    const size_t frame_size = sizeof(Frame) - sizeof(Value);
    unsigned required_value_slots = fn->get_function_slot_num()
            + fn->get_variable_slot_num() + fn->get_stack_size();
    uintptr_t istack_ptr = reinterpret_cast<uintptr_t>(stack_ptr);
    Value* local_area_begin = reinterpret_cast<Value*>(istack_ptr + frame_size);
    Value* func_slots = local_area_begin;
    Value* var_slots = local_area_begin + fn->get_function_slot_num();
    memset(local_area_begin, 0, sizeof(Value) * (fn->get_function_slot_num() + fn->get_variable_slot_num()));
    closure = Closure::create(closure_, argc_, args_, func_slots, var_slots);
    stack = local_area_begin + required_value_slots;
    uintptr_t istack = reinterpret_cast<uintptr_t>(stack);
    size_t align = alignment_of<Frame>::value;
    if (istack % align) {
        next_stack_ptr = reinterpret_cast<Value*>(
                    istack + align - (istack % align));
    } else {
        next_stack_ptr = stack;
    }

    if (next_stack_ptr > R->get_stack_end()) {
        fatal("Stack overflow");
    }
}

bool
Frame::local_func_ref(unsigned depth, unsigned offset, Value& value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    value = clos->get_func_slots()[offset];
    return true;
}

bool
Frame::local_func_set(unsigned depth, unsigned offset, Value value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    clos->get_func_slots()[offset] = value;
    return true;
}

bool
Frame::local_var_ref(unsigned depth, unsigned offset, Value& value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    value = clos->get_var_slots()[offset];
    return true;
}

bool
Frame::local_var_set(unsigned depth, unsigned offset, Value value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    clos->get_var_slots()[offset] = value;
    return true;
}

bool
Frame::local_arg_ref(unsigned depth, unsigned offset, Value& value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    value = clos->get_args()[offset];
    return true;
}

bool
Frame::local_arg_set(unsigned depth, unsigned offset, Value value) {
    Closure* clos = closure;
    for (; depth > 0; depth--, clos = clos->get_parent()) {
        if (clos == nullptr) { return false; }
    }

    clos->get_args()[offset] = value;
    return true;
}

State::State() {
    SwitchState ss(this); // Hack

    // Initialize symbol provider
    initialize_symbol();

    // Initialize allocator
    allocator_ = new (GC_malloc(sizeof(Allocator))) Allocator();

    // Setup namespace
    func_slots_ = SysTable<const Symbol*, Object*>::create();
    var_slots_ = SysTable<const Symbol*, Value>::create();
    klass_slots_ = SysTable<const Symbol*, ClassID>::create();

    // Bootstrap type system
    initialize_class();

    // Initialize operator
    initialize_operator();

    stack_top_ = stack_begin_ = allocate_raw_array(kStackSize);
    stack_end_ = stack_begin_ + kStackSize;
}

State::~State() {
}

void
State::initialize_symbol() {
    symbol_provider_ = SymbolProvider::create();
}

void
State::initialize_class() {
    klass_info_table_ = SysArray<ClassInfo*>::create();

    // Set parent
    // For Any
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::InvalidClassID,
                              get_symbol("Any"),
                              nullptr));
    // For Class
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Class"),
                              nullptr));
    // For Int
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Int"),
                              nullptr));
    // For Char
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Char"),
                              nullptr));
    // For Nil
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Nil"),
                              nullptr));
    // For Bool
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Bool"),
                              nullptr));
    // For Array
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Array"),
                              nullptr));
    // For Method
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Method"),
                              nullptr));
    // For BytecodeFunction
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("BytecodeFunction"),
                              nullptr));
    // For NativeFunction
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("NativeFunction"),
                              nullptr));
    // For HashTable
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("HashTable"),
                              nullptr));
    // For String
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("String"),
                              nullptr));
    // For Symbol
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("Symbol"),
                              nullptr));
    // For RestArguments
    klass_info_table_->append(
            ClassInfo::create(BuiltinClassID::Any,
                              get_symbol("RestArguments"),
                              nullptr));

    // Map class name to class id
    for (int i = BuiltinClassID::Any;
            i < BuiltinClassID::UserDefinedStart;
            i++) {
        Symbol* name = klass_info_table_->elt_ref(i)->name();
        klass_slots_->insert(name, i);
    }
}

void
State::initialize_operator() {
    unary_operator_table_ = SysArray<UnaryOperator>::create();
    binary_operator_table_ = SysArray<BinaryOperator>::create();

    unary_operator_table_->append(op_inc);
    unary_operator_table_->append(op_dec);
    unary_operator_table_->append(op_neg);
    unary_operator_table_->append(op_not);

    binary_operator_table_->append(op_add);
    binary_operator_table_->append(op_sub);
    binary_operator_table_->append(op_mul);
    binary_operator_table_->append(op_div);
    binary_operator_table_->append(op_mod);
    binary_operator_table_->append(op_eq);
    binary_operator_table_->append(op_ne);
    binary_operator_table_->append(op_gt);
    binary_operator_table_->append(op_lt);
    binary_operator_table_->append(op_ge);
    binary_operator_table_->append(op_le);
}

bool
State::get_class(Symbol* id, ClassID& klass) const {
    klass = klass_slots_->find(id);
    return true;
}

bool
State::get_class_name(ClassID klass, Symbol*& name) const {
    name = klass_info_table_->elt_ref((int)klass)->name();
    return true;
}

bool
State::get_class_parent(ClassID klass, ClassID& parent) const {
    parent = klass_info_table_->elt_ref((int)klass)->parent();
    return true;
}

bool
State::get_class_record_info(ClassID klass, RecordInfo*& record_info) const {
    record_info = klass_info_table_->elt_ref((int)klass)->record_info();
    if (record_info == nullptr) {
        return false;
    }
    return true;
}

bool
State::global_func_ref(Symbol* id, Value& func) const {
    func = Value::by_object(func_slots_->find(id));
    return true;
}

bool
State::global_var_ref(Symbol* id, Value& value) const {
    value = var_slots_->find(id);
    return true;
}

bool
State::global_var_set(Symbol* id, Value value) {
    var_slots_->insert(id, value);
    return true;
}

bool
State::add_function(Symbol* name, Function* func) {
    if (func_slots_->exists(name)) {
        Object* old = func_slots_->find(name);
        if (get_class_of(old) == BuiltinClassID::NativeFunction
                || get_class_of(old) == BuiltinClassID::BytecodeFunction) {
            Method* method = Method::create();
            method->add_function(static_cast<Function*>(old));
            method->add_function(func);
            func_slots_->assign(name, method);
            return true;
        } else if (get_class_of(old) == BuiltinClassID::Method) {
            return static_cast<Method*>(old)->add_function(func);
        } else {
            fatal("Error");
        }
    }

    func_slots_->insert_if_absent(name, func);
    return true;
}

ClassID
State::add_class(Symbol* name, ClassID parent, RecordInfo* record_info) {
    ClassInfo* class_info = ClassInfo::create(parent, name, record_info);
    ClassID new_id = klass_info_table_->get_length();
    klass_info_table_->append(class_info);
    klass_slots_->insert_if_absent(name, new_id);
    return new_id;
}


bool
State::add_variable(Symbol* id, Value val) {
    var_slots_->insert_if_absent(id, val);
    return true;
}

bool
State::load_module(Module* module) {
    return module->initialize();
}

UnaryOperator
State::get_unary_operator(int no) const {
    return unary_operator_table_->elt_ref(no);
}

BinaryOperator
State::get_binary_operator(int no) const {
    return binary_operator_table_->elt_ref(no);
}

void
State::dump_classes() {
    //klass_slots_->dump();
}

void
State::dump_functions() {
    //func_slots_->dump();
}

void
State::dump_variables() {
    //var_slots_->dump();
}

Value
execute(Symbol* entry_point, unsigned argc, Value* args) {
    Value fn;
    if (!get_current_state()->global_func_ref(entry_point, fn)) {
        fatal("No such function");
    }
    return execute(fn, argc, args);
}

Value
execute(Value fn, unsigned argc, Value* args) {
    if (!fn.is(Value::Type::Object)) {
        fatal("Not excutable object");
    }

    if (get_class_of(fn) == BuiltinClassID::Method) {
        if (!fn.get_obj<Method>()->dispatch(argc, args, fn)) {
            fatal("Could not dispatch");
        }
    }

    if (!fn.is(Value::Type::Object)) {
        fatal("Not excutable object");
    }

    Object* ofn = fn.get_obj<Object>();
    if (get_class_of(ofn) == BuiltinClassID::BytecodeFunction) {
        return execute((BytecodeFunction*)ofn, argc, args);
    } else if (get_class_of(ofn) == BuiltinClassID::NativeFunction) {
        return ((NativeFunction*)ofn)->get_body()(argc, args);
    } else {
        fatal("Not excutable object");
    }
    // NOTREACHED
    return Value::k_nil();
}

#define LOCAL_REFER_OP(op) { \
    uint32_t depth = get_insn_arg_uu1(insn); \
    uint32_t offset = get_insn_arg_uu2(insn); \
    if (!op(depth, offset, *(--sp))) { \
        fprintf(stderr, #op ":%u:%u\n", depth, offset); \
        fatal("Error on local refer"); \
    } \
    pc++; \
    } \
    break;

#define LOCAL_SET_OP(op) { \
    uint32_t depth = get_insn_arg_uu1(insn); \
    uint32_t offset = get_insn_arg_uu2(insn); \
    if (!op(depth, offset, *(sp))) { \
        fprintf(stderr, #op ":%u:%u\n", depth, offset); \
        fatal("Error on local set"); \
    } \
    pc++; \
    } \
    break;

#define GLOBAL_REFER_OP(op) { \
    Value id = fn->get_constant_table()[get_insn_arg_u(insn)]; \
    if (get_class_of(id) != BuiltinClassID::Symbol) { \
        fatal("Error on global refer"); \
    } \
    if (!R->op(id.get_obj<Symbol>(), *(--sp))) { \
        fatal("Error on global refer"); \
    } \
    pc++; \
    } \
    break;

#define GLOBAL_SET_OP(op) { \
    Value id = fn->get_constant_table()[get_insn_arg_u(insn)]; \
    if (get_class_of(id) != BuiltinClassID::Symbol) { \
        fatal("Error on global set"); \
    } \
    if (!R->op(id.get_obj<Symbol>(), *(sp))) { \
        fatal("Error on global set"); \
    } \
    pc++; \
    } \
    break;

Value
execute(BytecodeFunction* entry_fn, unsigned argc_, Value* args_) {
    State* R = get_current_state();
    Value*& next_stack_ptr = R->get_stack_top();
    Frame* fr = Frame::create(next_stack_ptr, entry_fn, nullptr,
            entry_fn->get_closure(), argc_, args_, next_stack_ptr);
    BytecodeFunction* fn = entry_fn;
    Value* sp = fr->stack;
    const uint32_t* pc = fn->get_bytecode();

    for(; ; ){
        uint32_t insn = *pc;
        switch(insn & 0xff) {
            case Insn::UnaryOp:
                if (!((*R->get_unary_operator(get_insn_arg_u(insn)))(sp[0], sp[0]))) {
                    fatal("Error");
                }
                pc++;
                break;
            case Insn::BinaryOp:
                if (!((*R->get_binary_operator(get_insn_arg_u(insn)))(sp[1], sp[0], sp[1]))) {
                    fatal("Error");
                }
                pc++;
                sp++;
                break;
            case Insn::Jump: {
                uint32_t dest = get_insn_arg_u(insn);
                pc = fn->get_bytecode() + dest;
            }
                break;
            case Insn::IfJump: {
                uint32_t dest = get_insn_arg_u(insn);
                if ((*sp++).like_true()) {
                    pc = fn->get_bytecode() + dest;
                } else {
                    pc++;
                }
            }
                break;
            case Insn::UnlessJump: {
                uint32_t dest = get_insn_arg_u(insn);
                if ((*sp++).like_false()) {
                    pc = fn->get_bytecode() + dest;
                } else {
                    pc++;
                }
            }
                break;
            case Insn::Call: {
                uint32_t argc = get_insn_arg_u(insn);
                Value func = *sp++;
                Value* stack_args = sp;

                if (!func.is(Value::Type::Object)) {
                    fatal("Not callable object");
                }

                Closure* closure = nullptr;
                if (get_class_of(func.get_obj<Object>()) == BuiltinClassID::Method) {
                    if (!func.get_obj<Method>()->dispatch(argc, stack_args, func)) {
                        fatal("Could not dispatch");
                    }
                    closure = func.get_obj<Method>()->get_closure();
                }

                Function* ofunc = func.get_obj<Function>();
                if (!ofunc->get_info()->check_type(argc, stack_args)) {
                    ofunc->get_info()->name()->dump();
                    fatal("Type error");
                }

                if (get_class_of(ofunc) == BuiltinClassID::BytecodeFunction) {
                    fn = (BytecodeFunction*)ofunc;

                    if (fn->get_info()->variadic()) {
                        unsigned vargs_count = argc - fn->get_info()->num_args();
                        RestArguments* vargs = RestArguments::create(vargs_count);
                        for (unsigned i = 0; i < vargs_count; i++) {
                            vargs->elt_set(i, stack_args[i + fn->get_info()->num_args()]);
                        }
                        stack_args[fn->get_info()->num_args()] = Value::by_object(vargs);
                    }

                    if (closure == nullptr) {
                        closure = fn->get_closure();
                    }

                    fr->pc = pc + 1;
                    fr->sp = sp + argc;
                    fr = Frame::create(next_stack_ptr, fn, fr, closure, argc,
                            stack_args, next_stack_ptr);
                    pc = fn->get_bytecode();
                    sp = fr->stack;
                } else if (get_class_of(ofunc) == BuiltinClassID::NativeFunction) {
                    Value ret = (*func.get_obj<NativeFunction>()->get_body())(argc, sp);
                    sp += argc;
                    *(--sp) = ret;
                    pc++;
                }
            }
                break;
            case Insn::Ret: {
                Frame* parent = fr->parent;
                if (fr->should_save_closure) {
                    fr->closure->copy_slots(fr->fn);
                }
                if (parent == nullptr) { // if top level
                    goto VMExit;
                }
                next_stack_ptr = fr->restore_stack_ptr;
                *(--parent->sp) = *sp;
                pc = parent->pc;
                sp = parent->sp;
                fr = parent;
                fn = parent->fn;
            }
                break;
            case Insn::Ranew: {
                Value size = *sp++;
                if (!size.is(Value::Type::Int) || size.get_int() < 0) {
                    fatal("Invalid size");
                }

                *(--sp) = Value::by_object(Array::create(size.get_int()));
                pc++;
            }
                break;
            case Insn::Raref: {
                Value index = *sp++;
                Value array = *sp;

                if (get_class_of(array) != BuiltinClassID::Array) {
                    fatal("Invalid type");
                }

                if (!array.get_obj<Array>()->index_ref(index, *(--sp))) {
                    fatal("Cannot refer");
                }
                pc++;
            }
                break;
            case Insn::Raset: {
                Value value = *sp++;
                Value index = sp[0];
                Value array = sp[1];

                if (get_class_of(array) != BuiltinClassID::Array) {
                    fatal("Invalid type");
                }

                if (!array.get_obj<Array>()->index_set(index, value)) {
                    fatal("Cannot set");
                }
                pc++;
            }
                break;
            case Insn::Iref: {
                Value index = *sp++;
                if (!sp[0].is(Value::Type::Object)) {
                    fatal("Cannot refer");
                }

                if (!sp[0].get_obj<Object>()->index_ref(index, sp[0])) {
                    fatal("Cannot refer");
                }
                pc++;
            }
                break;
            case Insn::Iset: {
                Value index = *sp++;
                Value obj = *sp++;
                Value value = sp[0];

                if (!obj.is(Value::Type::Object)) {
                    fatal("Cannot set");
                }

                if (!obj.get_obj<Object>()->index_set(index, value)) {
                    fatal("Cannot set");
                }
                pc++;
            }
                break;
            case Insn::Mref: {
                Value obj = sp[0];
                Value id = fn->get_constant_table()[get_insn_arg_u(insn)];

                if (get_class_of(id) != BuiltinClassID::Symbol) {
                    fatal("Cannot refer");
                }
                Symbol* id_sym = id.get_obj<Symbol>();

                if (obj.is(Value::Type::Object)
                        && !obj.get_obj<Object>()->slot_ref(id_sym, sp[0])) {
                    id.get_obj<Symbol>()->dump();
                    fatal("Cannot refer");
                } else if (obj.is(Value::Type::Record)
                        && !obj.get_rec()->slot_ref(id_sym, sp[0])) {
                    id.get_obj<Symbol>()->dump();
                    fatal("Cannot refer");
                }
                pc++;
            }
                break;
            case Insn::Mset: {
                Value obj = *sp++;
                Value id = fn->get_constant_table()[get_insn_arg_u(insn)];

                if (get_class_of(id) != BuiltinClassID::Symbol) {
                    fatal("Cannot set");
                }
                Symbol* id_sym = id.get_obj<Symbol>();

                if (obj.is(Value::Type::Object)
                        && !obj.get_obj<Object>()->slot_set(id_sym, sp[0])) {
                    fatal("Cannot set");
                } else if (obj.is(Value::Type::Record)
                        && !obj.get_rec()->slot_set(id_sym, sp[0])) {
                    fatal("Cannot set");
                }
                pc++;
            }
                break;
            case Insn::Lfref: LOCAL_REFER_OP(fr->local_func_ref)
            case Insn::Lfset: LOCAL_SET_OP(fr->local_func_set)
            case Insn::Lvref: LOCAL_REFER_OP(fr->local_var_ref)
            case Insn::Lvset: LOCAL_SET_OP(fr->local_var_set)
            case Insn::Laref: LOCAL_REFER_OP(fr->local_arg_ref)
            case Insn::Laset: LOCAL_SET_OP(fr->local_arg_set)
            case Insn::Gfref: GLOBAL_REFER_OP(global_func_ref)
            case Insn::Gvref: GLOBAL_REFER_OP(global_var_ref)
            case Insn::Gvset: GLOBAL_SET_OP(global_var_set)
            case Insn::Load:
                *(--sp) = fn->get_constant_table()[get_insn_arg_u(insn)];
                pc++;
                break;
            case Insn::LoadClass: {
                Value id = fn->get_constant_table()[get_insn_arg_u(insn)];

                if (get_class_of(id) != BuiltinClassID::Symbol) {
                    fatal("Name must be string");
                }

                ClassID class_id;
                if (!R->get_class(id.get_obj<Symbol>(), class_id)) {
                    fatal("Cannot find klass");
                }
                *(--sp) = Value::by_class(class_id);
                pc++;
            }
                break;
            case Insn::LoadUndef:
                *(--sp) = Value::k_undef();
                pc++;
                break;
            case Insn::LoadNull:
                *(--sp) = Value::k_nil();
                pc++;
                break;
            case Insn::LoadTrue:
                *(--sp) = Value::k_true();
                pc++;
                break;
            case Insn::LoadFalse:
                *(--sp) = Value::k_false();
                pc++;
                break;
            case Insn::Enclose: {
                Value id = fn->get_constant_table()[get_insn_arg_u(insn)];

                if (get_class_of(id) != BuiltinClassID::Symbol) {
                    fatal("Cannot enclose");
                }

                Value func;
                if (!R->global_func_ref(id.get_obj<Symbol>(), func)) {
                    fatal("Cannot enclose");
                }

                if (!func.is(Value::Type::Object)) {
                    fatal("Cannot enclose");
                }

                if (get_class_of(func) == BuiltinClassID::NativeFunction) {
                    *(--sp) = Value::by_object(func.get_obj<NativeFunction>()->enclose(fr->closure));
                } else if (get_class_of(func) == BuiltinClassID::BytecodeFunction) {
                    *(--sp) = Value::by_object(func.get_obj<BytecodeFunction>()->enclose(fr->closure));
                } else if (get_class_of(func) == BuiltinClassID::Method) {
                    *(--sp) = Value::by_object(func.get_obj<Method>()->enclose(fr->closure));
                } else {
                    fatal("Cannot enclose");
                }
                fr->should_save_closure = true;
                pc++;
            }
                break;
            case Insn::Dup:
                --sp;
                sp[0] = sp[1];
                pc++;
                break;
            case Insn::Pop:
                sp++;
                pc++;
                break;
            case Insn::Escape: {
                Value value = *sp++;
                sp += get_insn_arg_u(insn);
                *(--sp) = value;
                pc++;
            }
                break;
            case Insn::Break:
                getchar();
                pc++;
                break;
            default:
                fprintf(stderr, "%u\n", insn);
                fatal("Invalid insn");
                break;
        }
    }
VMExit:
    return *sp;
}


bool
add_native_function(const char* id,
                    bool variadic,
                    std::initializer_list<const char*> arg_class_names,
                    NativeFunctionBody body) {
    State* R = get_current_state();

    size_t num_args = arg_class_names.size();
    ClassID* arg_class_ids = R->allocate_block<ClassID>(num_args);
    FunctionInfo::ArgDispatchKind* arg_dispatch_kinds = R->allocate_block<FunctionInfo::ArgDispatchKind>(num_args);
    {
        int j = 0;
        for (auto i = arg_class_names.begin();
                i != arg_class_names.end();
                i++, j++) {
            ClassID class_id;
            if (!R->get_class(R->get_symbol(*i), class_id)) { return false; }
            arg_class_ids[j] = class_id;
            arg_dispatch_kinds[j] = FunctionInfo::ArgDispatchKind::Instance;
        }
    }

    return R->add_function(
            R->get_symbol(id),
            NativeFunction::create(
                FunctionInfo::create(
                    R->get_symbol(id),
                    variadic,
                    num_args,
                    arg_dispatch_kinds,
                    arg_class_ids),
                body));
}

ClassID
add_class(const char* name, const char* parent_name) {
    State* R = get_current_state();
    ClassID parent_id;
    if (!R->get_class(R->get_symbol(parent_name), parent_id)) {
        return BuiltinClassID::InvalidClassID;
    }
    return R->add_class(R->get_symbol(name), parent_id, nullptr);
}

bool
add_variable(const char* name, Value v) {
    State* R = get_current_state();
    return R->add_variable(R->get_symbol(name), v);
}

}

