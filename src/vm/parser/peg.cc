//
// peg.cc
//

#include "peg.h"
#include <iostream>

namespace rhein {
namespace peg {

ClassID peg_string_class_id;
ClassID peg_char_class_class_id;
ClassID peg_times_class_id;
ClassID peg_pred_class_id;
ClassID peg_sequence_class_id;
ClassID peg_choice_class_id;
ClassID peg_action_class_id;
ClassID peg_any_class_id;
ClassID peg_dynamic_class_id;
ClassID peg_try_class_id;
ClassID peg_drop_class_id;
ClassID peg_permute_class_id;
ClassID peg_null_class_id;
ClassID peg_constant_class_id;
ClassID peg_spacing_class_id;
ClassID peg_chain_left_class_id;
ClassID peg_skip_class_id;
ClassID peg_sep_by_class_id;
ClassID peg_unwrap_class_id;
ClassID peg_tree_to_string_class_id;
ClassID peg_string_to_symbol_class_id;

static String*
tree_to_string(Value obj) {
    if (get_class_of(obj) == BuiltinClassID::Array) {
        Array* ary = obj.get_obj<Array>();
        String* res = String::create("");
        for (int i = 0; i < ary->get_length(); i++) {
            res = res->append(tree_to_string(ary->elt_ref(i)));
        }
        return res;
    } else if (get_class_of(obj) == BuiltinClassID::String) {
        return obj.get_obj<String>();
    } else if (get_class_of(obj) == BuiltinClassID::Char) {
        char ch = obj.get_char();
        return String::create(ch);
    }

    fprintf(stderr, "Not suitable object\n");
    throw "";
}

bool
PegString::parse(List* src, Value&, Value& obj, List*& next) {
    List* prev = src;
    for (Int i = 0; i < str_->get_length(); i++) {
        if (src == end_of_list) {
            next = prev;
            obj = Value::k_nil();
            return false;
        }

        char str_ch = str_->elt_ref(i);
        char src_ch = src->get_head().get_char();
        if (str_ch != src_ch) {
            next = prev;
            obj = Value::k_nil();
            return false;
        }

        prev = src;

        Value tail_v = src->get_tail();
        src = tail_v.get_obj<List>();
    }

    next = src;
    obj = Value::by_object(str_);
    return true;
}

void
PegCharClass::add(char begin, char end) {
    end++;
    int b_x = begin / kIntBits;
    int b_y = begin % kIntBits;
    int e_x = end / kIntBits;
    int e_y = end % kIntBits;
    if (b_x == e_x) {
        for (int i = b_y; i < e_y; i++) {
            table_[b_x] |= 1 << i;
        }
    } else {
        for (int i = b_y; i < static_cast<int>(kIntBits); i++) {
            table_[b_x] |= 1 << i;
        }
        for (int i = b_x + 1; i < e_x - 1; i++) {
            table_[i]   =  0xffffffff;
        }
        for (int i = 0; i < e_y; i++) {
            table_[e_x] |= 1 << i;
        }
    }
}

void
PegCharClass::invert() {
    inverted_ = !inverted_;
}

bool
PegCharClass::parse(List* src, Value&, Value& obj, List*& next) {
    if (src == end_of_list) {
        obj = Value::k_nil();
        next = end_of_list;
        return false;
    }

    Value o = src->get_head();
    char ch = o.get_char();
    unsigned x = ch / kIntBits;
    unsigned y = ch % kIntBits;

    if (static_cast<bool>((table_[x] >> y) & 1) != inverted_) {
        next = src->get_tail().get_obj<List>();
        obj = Value::by_char(ch);
        return true;
    }

    // else
    next = src;
    obj = Value::k_nil();
    return false;
}

bool
PegTimes::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Array* ary = Array::create(0);
    Int i = 0;
    List* s = src;
    List* n = src;
    Value buf;

    for (; i < lower_; i++) {
        if (!syn_->parse(s, ctx, buf, s)) {
            next = s;
            obj = Value::k_nil();
            return false;
        }
        ary->append(buf);
    }

    for (; i != upper_; i++) {
        if (!syn_->parse(s, ctx, buf, n)) {
            break;
        }
        ary->append(buf);
        s = n;
    }

    obj = Value::by_object(ary);
    next = s;
    return true;
}

bool
PegPred::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* dummy_next;
    bool res = syn_->parse(src, ctx, obj, dummy_next);
    next = src;
    if (res == if_success_) {
        obj = Value::k_nil();
        return true;
    }

    // else
    obj = Value::k_nil();
    return false;
}

bool
PegSequence::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;
    Array* ary = Array::create(0);
    Value buf;

    next = src;
    for (Int i = 0; i < num_; i++) {
        if (!syns_[i]->parse(s, ctx, buf, next)) {
            obj = Value::k_nil();
            return false;
        }
        ary->append(buf);
        s = next;
    }

    obj = Value::by_object(ary);
    return true;
}

bool
PegChoice::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;
    next = src;
    obj = Value::k_nil();

    for (Int i = 0; i < num_; i++) {
        if (syns_[i]->parse(s, ctx, obj, next)) {
            return true;
        } else {
            // Automatic backtrack enabled
#if 0
            // Check if parser head did not go ahead
            if (s == next) { continue; }
            // else
            break;
#endif
        }
    }

    return false;
}

bool
PegAction::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Value syn_obj[1];
    if (syn_->parse(src, ctx, syn_obj[0], next)) {
        obj = execute(action_, 1, syn_obj);
        return true;
    }

    return false;
}

bool
PegAny::parse(List* src, Value&, Value& obj, List*& next) {
    if (src == end_of_list) {
        obj = Value::k_nil();
        next = end_of_list;
        return false;
    }
    obj = src->get_head();
    next = src->get_tail().get_obj<List>();
    return true;
}

bool
PegDynamic::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Value args[2];
    args[0] = ctx;
    args[1] = Value::by_object(src);
    obj = execute(fn_, 2, args);
    if (get_class_of(obj) != BuiltinClassID::Array) {
        fprintf(stderr, "Type error\n");
        return false;
    }

    Array* ary = obj.get_obj<Array>();

    Value v_succ, v_next;
    v_succ = ary->elt_ref(0);
    obj = ary->elt_ref(1);
    v_next = ary->elt_ref(2);
    next = v_next.get_obj<List>();

    return v_succ.get_bool();
}

bool
PegTry::parse(List* src, Value& ctx, Value& obj, List*& next) {
    if (!syn_->parse(src, ctx, obj, next)) {
        next = src;
        return false;
    }

    return true;
}

bool
PegDrop::parse(List* src, Value& ctx, Value& obj, List*& next) {
    if (!syn_->parse(src, ctx, obj, next)) {
        return false;
    }

    if (get_class_of(obj) != BuiltinClassID::Array) {
        fprintf(stderr, "Type error\n");
        return false;
    }

    obj.get_obj<Array>()->drop(num_);
    return true;
}

void
PegPermute::add(int index) {
    permute_table_->append(Value::by_int(index));
}

bool
PegPermute::parse(List* src, Value& ctx, Value& obj, List*& next) {
    if (!syn_->parse(src, ctx, obj, next)) {
        return false;
    }

    if (get_class_of(obj) != BuiltinClassID::Array) {
        fprintf(stderr, "Type error\n");
        return false;
    }

    Array* obj_ary = obj.get_obj<Array>();
    Array* res = Array::create(permute_table_->get_length());

    for (int i = 0; i < permute_table_->get_length(); i++) {
        int x = permute_table_->elt_ref(i).get_int();
        res->elt_set(i, obj_ary->elt_ref(x));
    }

    obj = Value::by_object(res);
    return true;
}

bool
PegNull::parse(List* src, Value& /* ctx */, Value& obj, List*& next) {
    obj = Value::k_nil();
    next = src;
    return true;
}

bool
PegConstant::parse(List* src, Value& ctx, Value& obj, List*& next) {
    if (!syn_->parse(src, ctx, obj, next)) {
        return false;
    }

    obj = value_;
    return true;
}

bool
PegSpacing::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;
    Array* ary = Array::create(0);
    Value buf;

    next = src;
    for (Int i = 0; i < num_; i++) {
        if (begin_ <= i && i <= end_) {
            if (space_->parse(s, ctx, buf, next)) {
                s = next;
            }
        }

        if (!syns_[i]->parse(s, ctx, buf, next)) {
            obj = Value::k_nil();
            return false;
        }

        ary->append(buf);
        s = next;
    }

    if (num_ <= end_) {
        if (space_->parse(s, ctx, buf, next)) {
            s = next;
        }
    }

    obj = Value::by_object(ary);
    return true;
}

bool
PegChainLeft::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;

    Value left;
    if (!syn_->parse(s, ctx, left, next)) {
        obj = Value::k_nil();
        return false;
    }
    s = next;

    while (true) {
        List* t;
        List* u;
        Value op_obj, right;

        if (!op_->parse(s, ctx, op_obj, t)) { break; }
        if (!syn_->parse(t, ctx, right, u)) { break; }

        Array* ary = Array::create(4);
        ary->elt_set(0, tag_);
        ary->elt_set(1, op_obj);
        ary->elt_set(2, left);
        ary->elt_set(3, right);

        left = Value::by_object(ary);
        s = u;
    }

    next = s;
    obj = left;
    return true;
}

bool
PegSkip::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;
    Value buf;

    if (pre_->parse(s, ctx, buf, next)) { s = next; }
    if (!syn_->parse(s, ctx, obj, next)) {
        obj = Value::k_nil();
        return false;
    }
    s = next;
    if (!post_->parse(s, ctx, buf, next)) { next = s; }
    return true;
}

bool
PegSepBy::parse(List* src, Value& ctx, Value& obj, List*& next) {
    List* s = src;
    List* p = src;
    Array* ary = Array::create(0);
    Value buf;

    for (int i = 0; ; i++) {
        if (i == upper_ || !syn_->parse(s, ctx, buf, next)) {
            if (end_) { next = s; }
            else { next = p; }
            if (i >= lower_) { goto success; }
            else { goto failure; }
        }

        ary->append(buf);
        p = next;

        if (!sep_->parse(p, ctx, buf, next)) {
            if (i >= lower_ || (!end_ && i >= lower_ - 1)) {
                next = p;
                goto success;
            }
            else { goto failure; }
        }

        s = next;
    }

success:
    obj = Value::by_object(ary);
    return true;

failure:
    obj = Value::k_nil();
    return false;
}

bool
PegUnwrap::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Value buf;
    if (!syn_->parse(src, ctx, buf, next)) {
        obj = Value::k_nil();
        return false;
    }

    if (get_class_of(buf) != BuiltinClassID::Array) {
        fprintf(stderr, "Result object is not array\n");
        return false;
    }

    obj = buf.get_obj<Array>()->elt_ref(index_);
    return true;
}

bool
PegTreeToString::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Value buf;
    if (!syn_->parse(src, ctx, buf, next)) {
        obj = Value::k_nil();
        return false;
    }

    obj = Value::by_object(tree_to_string(buf));
    return true;
}

bool
PegStringToSymbol::parse(List* src, Value& ctx, Value& obj, List*& next) {
    Value buf;
    if (!syn_->parse(src, ctx, buf, next)) {
        obj = Value::k_nil();
        return false;
    }

    if (get_class_of(buf) != BuiltinClassID::String) {
        fprintf(stderr, "Result object is not string\n");
        return false;
    }

    obj = Value::by_object(buf.get_obj<String>()->to_symbol());
    return true;

}

Value
fn_parse(unsigned /* argc */, Value* args) {
    Value res;
    bool succ;
    List* rest;
    PegSyntax* syn = args[0].get_obj<PegSyntax>();
    Value ctx = args[1];
    List* src = args[2].get_obj<List>();
    Array* ary = Array::create(3);

    succ = syn->parse(src, ctx, res, rest);

    ary->elt_set(0, Value::by_bool(succ));
    ary->elt_set(1, res);
    if (rest != end_of_list) {
        ary->elt_set(2, Value::by_object(rest));
    } else {
        ary->elt_set(2, Value::by_object(end_of_list));
    }
    return Value::by_object(ary);
}

Value
fn_pstr(unsigned /* argc */, Value* args) {
    return Value::by_object(PegString::create(args[0].get_obj<String>()));
}

Value
fn_pchar(unsigned /* argc */, Value* /* args */) {
    return Value::by_object(PegCharClass::create());
}

Value
fn_pchar_add1(unsigned, Value* args) {
    args[0].get_obj<PegCharClass>()->add(args[1].get_char(), args[1].get_char());
    return args[0];
}

Value
fn_pchar_add(unsigned /* argc */, Value* args) {
    args[0].get_obj<PegCharClass>()->add(args[1].get_char(),
            args[2].get_char());
    return args[0];
}

Value
fn_pchar_inv(unsigned /* argc */, Value* args) {
    args[0].get_obj<PegCharClass>()->invert();
    return args[0];
}

Value
fn_star(unsigned /* argc */, Value* args) {
    return Value::by_object(PegTimes::create( 0, -1,
                args[0].get_obj<PegSyntax>()));
}

Value
fn_plus(unsigned /* argc */, Value* args) {
    return Value::by_object(PegTimes::create( 1, -1,
                args[0].get_obj<PegSyntax>()));
}

Value
fn_times(unsigned /* argc */, Value* args) {
    return Value::by_object(
            PegTimes::create(
                args[1].get_int(), args[2].get_int(),
                args[0].get_obj<PegSyntax>()));
}

Value
fn_opt(unsigned /* argc */, Value* args) {
    return Value::by_object(
            PegTimes::create( 0, 1,
                args[0].get_obj<PegSyntax>()));
}

Value
fn_andp(unsigned /* argc */, Value* args) {
    return Value::by_object(
            PegPred::create(true, args[0].get_obj<PegSyntax>()));
}

Value
fn_notp(unsigned /* argc */, Value* args) {
    return Value::by_object(
            PegPred::create(false, args[0].get_obj<PegSyntax>()));
}

Value
fn_pseq(unsigned argc, Value* args) {
    State* R = get_current_state();
    PegSyntax** syns = R->allocate_block<PegSyntax*>(argc);
    for (unsigned i = 0; i < argc; i++) {
        syns[i] = args[i].get_obj<PegSyntax>();
    }
    return Value::by_object(PegSequence::create(argc, syns));
}

Value
fn_pchoice(unsigned argc, Value* args) {
    State* R = get_current_state();
    PegSyntax** syns = R->allocate_block<PegSyntax*>(argc);
    for (unsigned i = 0; i < argc; i++) {
        syns[i] = args[i].get_obj<PegSyntax>();
    }
    return Value::by_object(PegChoice::create(argc, syns));
}

Value
fn_paction(unsigned /* argc */, Value* args) {
    return Value::by_object(PegAction::create(args[1],
                args[0].get_obj<PegSyntax>()));
}

Value
fn_pany(unsigned /* argc */, Value* /* args */) {
    return Value::by_object(PegAny::create());
}

Value
fn_pdynamic(unsigned /* argc */, Value* args) {
    return Value::by_object(PegDynamic::create(args[0]));
}

Value
fn_ptry(unsigned /* argc */, Value* args) {
    return Value::by_object(PegTry::create(args[0].get_obj<PegSyntax>()));
}

Value
fn_pdrop(unsigned /* argc */, Value* args) {
    return Value::by_object(PegDrop::create(args[0].get_obj<PegSyntax>(),
                args[1].get_int()));
}

Value
fn_pperm(unsigned argc, Value* args) {
    PegPermute* perm = PegPermute::create(args[0].get_obj<PegSyntax>());
    for (unsigned i = 1; i < argc; i++) {
        Value index_v = args[i];
        if (!index_v.is(Value::Type::Int)) {
            fprintf(stderr, "pperm: Integer required\n");
            return Value::k_nil();
        }
        perm->add(index_v.get_int());
    }
    return Value::by_object(perm);
}

Value
fn_pconst(unsigned /* argc */, Value* args) {
    return Value::by_object(PegConstant::create(args[0].get_obj<PegSyntax>(),
            args[1]));
}

Value
fn_psseq(unsigned argc, Value* args) {
    State* R = get_current_state();
    PegSyntax** syns = R->allocate_block<PegSyntax*>(argc - 3);
    for (unsigned i = 0; i + 3 < argc; i++) {
        syns[i] = args[i + 3].get_obj<PegSyntax>();
    }
    return Value::by_object(PegSpacing::create(argc - 3, syns, args[0].get_obj<PegSyntax>(), args[1].get_int(), args[2].get_int()));
}

Value
fn_pchain_left(unsigned /* argc */, Value* args) {
    return Value::by_object(PegChainLeft::create(args[0].get_obj<PegSyntax>(), args[1].get_obj<PegSyntax>(), args[2]));
}

Value
fn_pskip(unsigned /* argc */, Value* args) {
    return Value::by_object(PegSkip::create(
                args[0].get_obj<PegSyntax>(),
                args[1].get_obj<PegSyntax>(),
                args[2].get_obj<PegSyntax>()));
}

Value
fn_psepby(unsigned /* argc */, Value* args) {
    return Value::by_object(PegSepBy::create(
                args[0].get_obj<PegSyntax>(),
                args[1].get_obj<PegSyntax>(),
                args[2].get_int(),
                args[3].get_int(),
                args[4].get_bool()));
}

Value
fn_unwrap(unsigned /* argc */, Value* args) {
    return Value::by_object(PegUnwrap::create(
                args[0].get_obj<PegSyntax>(),
                args[1].get_int()));
}

Value
fn_t2s(unsigned /* argc */, Value* args) {
    return Value::by_object(PegTreeToString::create(
                args[0].get_obj<PegSyntax>()));
}

Value
fn_s2s(unsigned /* argc */, Value* args) {
    return Value::by_object(PegStringToSymbol::create(
                args[0].get_obj<PegSyntax>()));
}

PegModule*
PegModule::create() {
    State* R = get_current_state();
    void* p = R->allocate_struct<PegModule>();
    return new (p) PegModule;
}

bool
PegModule::initialize() {
    add_class("PegSyntax", "Any");
    peg_string_class_id = add_class("PegString", "PegSyntax");
    peg_char_class_class_id = add_class("PegCharClass", "PegSyntax");
    peg_times_class_id = add_class("PegTimes", "PegSyntax");
    peg_pred_class_id = add_class("PegPred", "PegSyntax");
    peg_sequence_class_id = add_class("PegSequence", "PegSyntax");
    peg_choice_class_id = add_class("PegChoice", "PegSyntax");
    peg_action_class_id = add_class("PegAction", "PegSyntax");
    peg_any_class_id = add_class("PegAny", "PegSyntax");
    peg_dynamic_class_id = add_class("PegDynamic", "PegSyntax");
    peg_try_class_id = add_class("PegTry", "PegSyntax");
    peg_drop_class_id = add_class("PegDrop", "PegSyntax");
    peg_permute_class_id = add_class("PegPermute", "PegSyntax");
    peg_null_class_id = add_class("PegNull", "PegSyntax");
    peg_constant_class_id = add_class("PegConstant", "PegSyntax");
    peg_spacing_class_id = add_class("PegSpacing", "PegSyntax");
    peg_chain_left_class_id = add_class("PegChainLeft", "PegSyntax");
    peg_skip_class_id = add_class("PegSkip", "PegSyntax");
    peg_sep_by_class_id = add_class("PegSepBy", "PegSyntax");
    peg_unwrap_class_id = add_class("PegUnwrap", "PegSyntax");
    peg_tree_to_string_class_id = add_class("PegTreeToString", "PegSyntax");
    peg_string_to_symbol_class_id = add_class("PegStringToSymbol", "PegSyntax");

    add_native_function("parse", false, {"PegSyntax", "Any", "List"}, fn_parse);
    add_native_function("pstr", false, {"String"}, fn_pstr);
    add_native_function("pchar", false, { }, fn_pchar);
    add_native_function("add", false, {"PegCharClass", "Char", "Char"}, fn_pchar_add);
    add_native_function("add", false, {"PegCharClass", "Char"}, fn_pchar_add1);
    add_native_function("inv", false, {"PegCharClass"}, fn_pchar_inv);
    add_native_function("star", false, {"PegSyntax"}, fn_star);
    add_native_function("plus", false, {"PegSyntax"}, fn_plus);
    add_native_function("times", false, {"PegSyntax", "Int", "Int"}, fn_times);
    add_native_function("opt", false, {"PegSyntax"}, fn_opt);
    add_native_function("andp", false, {"PegSyntax"}, fn_andp);
    add_native_function("notp", false, {"PegSyntax"}, fn_notp);
    add_native_function("pseq", true, {}, fn_pseq);
    add_native_function("pchoice", true, {}, fn_pchoice);
    add_native_function("paction", false, {"PegSyntax", "Any"}, fn_paction);
    add_native_function("pdynamic", false, {"Any"}, fn_pdynamic);
    add_native_function("pany", false, {}, fn_pany);
    add_native_function("ptry", false, {"PegSyntax"}, fn_ptry);
    add_native_function("pdrop", false, {"PegSyntax", "Int"}, fn_pdrop);
    add_native_function("pperm", true, {"PegSyntax"}, fn_pperm);
    add_native_function("pconst", false, {"PegSyntax", "Any"}, fn_pconst);
    add_native_function("psseq", true, {"PegSyntax", "Int", "Int"}, fn_psseq);
    add_native_function("pchain_left", false, {"PegSyntax", "PegSyntax", "Any"}, fn_pchain_left);
    add_native_function("pskip", false, {"PegSyntax", "PegSyntax", "PegSyntax"}, fn_pskip);
    add_native_function("psepby", false, {"PegSyntax", "PegSyntax", "Int", "Int", "Bool"}, fn_psepby);
    add_native_function("unwrap", false, {"PegSyntax", "Int"}, fn_unwrap);
    add_native_function("t2s", false, {"PegSyntax"}, fn_t2s);
    add_native_function("s2s", false, {"PegSyntax"}, fn_s2s);

    PegNull* pnull = PegNull::create();
    add_variable("pnull", Value::by_object(pnull));
    return false;
}

}
}

