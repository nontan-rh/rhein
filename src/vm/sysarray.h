/*
 * sysarray.h - Dynamic array for system
 *
 * SysArray is a dynamic array implementation like std::vector
 */

#ifndef SYSARRAY_H
#define SYSARRAY_H

#include <stdexcept>

#include "vm.h"
#include "internal.h"
#include "object.h"

namespace rhein {

template <typename V>
class SysArray : public PlacementNewObj {
public:
    static SysArray* create() {
        return create(0);
    }

    static SysArray* create(int length) {
        return new (get_current_state()->allocate_struct<SysArray<V>>()) SysArray<V>(length);
    }

    int get_length() { return length_; }

    V elt_ref(int index) {
        if (0 > index || length_ <= index) {
            throw std::runtime_error("boundery error");
        }
        return array_[index];
    }

    void elt_set(int index, const V& value) {
        if (0 > index || length_ <= index) {
            throw std::runtime_error("boundery error");
        }
        array_[index] = value;
    }

    void append(const V& value) {
        if (length_ + 1 >= allocated_size_) {
            if (allocated_size_ == 0) {
                allocated_size_ = 1;
            } else {
                allocated_size_ *= 2;
            }

            V* new_array_ = get_current_state()->allocate_block<V>(allocated_size_);
            for (int i = 0; i < length_; i++) {
                new_array_[i] = array_[i];
            }
            get_current_state()->release_block(array_);

            array_ = new_array_;
        }

        array_[length_] = value;
        ++length_;
    }

private:
    SysArray(int initial_length) : length_(initial_length) {
        if (initial_length < 0) {
            throw std::logic_error("size must be positive or zero");
        }

        if (initial_length == 0) {
            allocated_size_ = 0;
            array_ = nullptr;
        } else {
            int s;
            for (s = 1; s > initial_length; s *= 2);
            allocated_size_ = s;
            array_ = get_current_state()->allocate_block<V>(s);
        }
    }

    int length_;
    int allocated_size_;

    V* array_;
};

}

#endif // For include guard
