//
// bytecode_module.h - Managing dynamic code generation and output
//

#ifndef BYTECODE_MODULE_H
#define BYTECODE_MODULE_H

#include <cstdint>

#include <vector>

#include "object.h"
#include "vm.h"

namespace rhein {

class FunctionBuilder : public Object {
public:
    static FunctionBuilder* create();

    // Information setter
    void set_name(Symbol* name) { name_ = name; }
    void set_id(Symbol* id) { id_ = id; }
    void set_variadic(bool is_variadic) { variadic_ = is_variadic; }
    void set_num_args(int num_args);
    void set_arg_type(int index, Symbol* class_id);
    void set_num_func_slots(int num_func_slots);
    void set_num_var_slots(int num_var_slots);
    void set_parameter_dispatch_type_list(Array* array);
    void set_parameter_class_list(Array* array);

    // Information getter
    Symbol* get_id() const { return id_; };

    // Instruction emitting method
    void unary_op(int op_id);
    void binary_op(int op_id);

    void jump(int label);
    void if_jump(int label);
    void unless_jump(int label);

    void call(int num_args);
    void ret();

    void ranew();
    void raref();
    void raset();

    void iref();
    void iset();

    void mref(Symbol* slot_id);
    void mset(Symbol* slot_id);

    void lfref(int depth, int offset);
    void lfset(int depth, int offset);
    void lvref(int depth, int offset);
    void lvset(int depth, int offset);
    void laset(int depth, int offset);
    void laref(int depth, int offset);

    void gfref(Symbol* codeobj_id);
    void gvref(Symbol* codeobj_id);
    void gvset(Symbol* codeobj_id);
    void enclose(Symbol* codeobj_id);

    void dup();
    void pop();
    void escape(int num);

    void load(Value obj);
    void load_klass(Symbol* klass_id);
    void load_undef();
    void load_nil();
    void load_true();
    void load_false();

    int generate_label_id();
    void label(int label_id);

    // Output
    BytecodeFunction* build_function();

    bool slot_ref(Symbol* slot_id, Value& value);

    void dump();

    bool store(FILE* fp);

private:
    FunctionBuilder();

    int get_constant_index(Value v);

    void get_required_stack_size_aux(int *st, int c, int h);
    unsigned get_required_stack_size();

    bool resolve_labels();

    Symbol* name_;
    Symbol* id_;
    int num_args_;
    bool variadic_;
    FunctionInfo::ArgDispatchKind* disp_kinds_;
    Symbol** argument_types_;
    SysArray<uint32_t>* insn_array_;
    HashTable* constant_index_table_;
    Array* constant_table_;
    SysTable<int, int>* label_table_;
    int func_slot_size_;
    int var_slot_size_;
    int next_label_id_;
    int stack_size_;
};

class ClassBuilder : public Object {
public:
    static ClassBuilder* create();

    void set_name(Symbol* name) { name_ = name; }
    void set_id(Symbol* id) { id_ = id; }
    void set_parent_name(Symbol* parent_name) { parent_name_ = parent_name; }
    void add_slot(Symbol* slot_name);

    Symbol* get_id() { return id_; }

    void dump();

    bool store(FILE* fp);

private:
    ClassBuilder();

    Symbol* name_;
    Symbol* id_;
    Symbol* parent_name_;
    int num_slots_;
    SysArray<Symbol*>* slots_;
};

class ModuleBuilder : public Object, public Module {
public:
    static ModuleBuilder* create();

    bool initialize();
    void write_to_file(FILE *fp);

    void register_function(FunctionBuilder* function_builder);
    void register_class(ClassBuilder* class_builder);
    void register_variable(Symbol* name);
    void set_initializer(Symbol* name);

    void dump();

    bool store(FILE* fp);

private:
    ModuleBuilder();

    SysArray<FunctionBuilder*>* functions_;
    SysArray<ClassBuilder*>* classes_;
    SysArray<Symbol*>* variables_;
    Symbol* initializer_;
};

class BytecodeModule : public Module, public PlacementNewObj {
public:
    static BytecodeModule* create();

    bool initialize();

private:
    BytecodeModule() = default;
};

}

#endif // For include guard
