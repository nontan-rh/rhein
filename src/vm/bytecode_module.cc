//
// bytecode_module.cc - Managing dynamic code generation and output
//

#include <cstdint>

#include <stdexcept>
#include <vector>

#include "object.h"
#include "vm.h"
#include "systable.h"
#include "sysarray.h"
#include "instruction.h"
#include "byteio.h"

#include "bytecode_module.h"

namespace rhein {

ClassID function_builder_class_id;
ClassID class_builder_class_id;
ClassID module_builder_class_id;

FunctionBuilder*
FunctionBuilder::create() {
    void *p = get_current_state()->allocate_object<FunctionBuilder>();
    return new (p) FunctionBuilder();
}

FunctionBuilder::FunctionBuilder()
    : Object(function_builder_class_id), name_(nullptr), id_(nullptr),
      num_args_(-1), argument_types_(nullptr) {

    insn_array_ = SysArray<uint32_t>::create();

    constant_index_table_ = HashTable::create();
    constant_table_ = Array::create(0);
    label_table_ = SysTable<int, int>::create();
    next_label_id_ = 1;
}

void
FunctionBuilder::set_num_args(int num_args) {
    if (num_args_ != -1) {
        throw std::logic_error("num_args is already set");
    }
    if (num_args < 0) {
        throw std::logic_error("num_args must be positive or zero");
    }
    num_args_ = num_args;
    argument_types_ = get_current_state()->allocate_block<Symbol*>(num_args);
}

void
FunctionBuilder::set_arg_type(int index, Symbol* class_id) {
    if (0 > index || index >= num_args_ ) {
        throw std::logic_error("boundary error");
    }
    argument_types_[index] = class_id;
}

void
FunctionBuilder::set_num_func_slots(int num_func_slots) {
    func_slot_size_ = num_func_slots;
}

void
FunctionBuilder::set_num_var_slots(int num_var_slots) {
    var_slot_size_ = num_var_slots;
}

void
FunctionBuilder::unary_op(int op_id) {
    insn_array_->append(encode_insn_arg1(Insn::UnaryOp, op_id));
}

void
FunctionBuilder::binary_op(int op_id) {
    insn_array_->append(encode_insn_arg1(Insn::BinaryOp, op_id));
}

void
FunctionBuilder::jump(int label) {
    insn_array_->append(encode_insn_arg1(Insn::Jump, label));
}

void
FunctionBuilder::if_jump(int label) {
    insn_array_->append(encode_insn_arg1(Insn::IfJump, label));
}

void
FunctionBuilder::unless_jump(int label) {
    insn_array_->append(encode_insn_arg1(Insn::UnlessJump, label));
}

void
FunctionBuilder::call(int num_args) {
    insn_array_->append(encode_insn_arg1(Insn::Call, num_args));
}

void
FunctionBuilder::ret() {
    insn_array_->append(Insn::Ret);
}

void
FunctionBuilder::ranew() {
    insn_array_->append(Insn::Ranew);
}

void
FunctionBuilder::raref() {
    insn_array_->append(Insn::Raref);
}

void
FunctionBuilder::raset() {
    insn_array_->append(Insn::Raset);
}

void
FunctionBuilder::iref() {
    insn_array_->append(Insn::Iref);
}

void
FunctionBuilder::iset() {
    insn_array_->append(Insn::Iset);
}

void
FunctionBuilder::mref(Symbol* slot_id) {
    int index = get_constant_index(Value::by_object(slot_id));
    insn_array_->append(encode_insn_arg1(Insn::Mref, index));
}

void
FunctionBuilder::mset(Symbol* slot_id) {
    int index = get_constant_index(Value::by_object(slot_id));
    insn_array_->append(encode_insn_arg1(Insn::Mset, index));
}

void
FunctionBuilder::lfref(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Lfref, depth, offset));
}

void
FunctionBuilder::lfset(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Lfset, depth, offset));
}

void
FunctionBuilder::lvref(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Lvref, depth, offset));
}

void
FunctionBuilder::lvset(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Lvset, depth, offset));
}

void
FunctionBuilder::laref(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Laref, depth, offset));
}

void
FunctionBuilder::laset(int depth, int offset) {
    insn_array_->append(encode_insn_arg2(Insn::Laset, depth, offset));
}

void
FunctionBuilder::gfref(Symbol* codeobj_id) {
    int index = get_constant_index(Value::by_object(codeobj_id));
    insn_array_->append(encode_insn_arg1(Insn::Gfref, index));
}

void
FunctionBuilder::gvref(Symbol* codeobj_id) {
    int index = get_constant_index(Value::by_object(codeobj_id));
    insn_array_->append(encode_insn_arg1(Insn::Gvref, index));
}

void
FunctionBuilder::gvset(Symbol* codeobj_id) {
    int index = get_constant_index(Value::by_object(codeobj_id));
    insn_array_->append(encode_insn_arg1(Insn::Gvset, index));
}

void
FunctionBuilder::enclose(Symbol* codeobj_id) {
    int index = get_constant_index(Value::by_object(codeobj_id));
    insn_array_->append(encode_insn_arg1(Insn::Enclose, index));
}

void
FunctionBuilder::dup() {
    insn_array_->append(Insn::Dup);
}

void
FunctionBuilder::pop() {
    insn_array_->append(Insn::Pop);
}

void
FunctionBuilder::escape(int num) {
    insn_array_->append(encode_insn_arg1(Insn::Escape, num));
}

void
FunctionBuilder::load(Value obj) {
    int index = get_constant_index(obj);
    insn_array_->append(encode_insn_arg1(Insn::Load, index));
}

void
FunctionBuilder::load_klass(Symbol* klass_id) {
    int index = get_constant_index(Value::by_object(klass_id));
    insn_array_->append(encode_insn_arg1(Insn::LoadClass, index));
}

void
FunctionBuilder::load_undef() {
    insn_array_->append(Insn::LoadUndef);
}

void
FunctionBuilder::load_nil() {
    insn_array_->append(Insn::LoadNull);
}

void
FunctionBuilder::load_true() {
    insn_array_->append(Insn::LoadTrue);
}

void
FunctionBuilder::load_false() {
    insn_array_->append(Insn::LoadFalse);
}

void
FunctionBuilder::label(int label_id) {
    if (label_table_->exists(label_id)) {
        throw std::logic_error("Label id collision");
    }
    fprintf(stderr, "%d -> %d\n", label_id, insn_array_->get_length());
    label_table_->insert(label_id, insn_array_->get_length());
}

int
FunctionBuilder::generate_label_id() {
    return next_label_id_++;
}

BytecodeFunction*
FunctionBuilder::build_function() {
    resolve_labels();

    stack_size_ = get_required_stack_size();

    unsigned constant_table_size = constant_table_->get_length();
    Value* raw_constant_table = get_current_state()->allocate_raw_array(constant_table_size);
    for (unsigned i = 0; i < constant_table_size; i++) {
        raw_constant_table[i] = constant_table_->elt_ref(i);
    }

    unsigned bytecode_size = insn_array_->get_length();
    uint32_t* bytecode = get_current_state()->allocate_block<uint32_t>(bytecode_size);
    for (unsigned i = 0; i < bytecode_size; i++) {
        bytecode[i] = insn_array_->elt_ref(i);
    }

    ClassID* arg_class_ids = get_current_state()->allocate_block<ClassID>(num_args_);
    for (int i = 0; i < num_args_; i++) {
        ClassID klass;
        //get_current_state()->get_class(argument_types_[i], klass);
        arg_class_ids[i] = klass;
    }

    BytecodeFunction* bc = BytecodeFunction::create(
            FunctionInfo::create(
                id_,
                variadic_,
                num_args_,
                disp_kinds_,
                arg_class_ids),
            func_slot_size_,
            var_slot_size_,
            stack_size_,
            constant_table_size,
            raw_constant_table,
            bytecode_size,
            bytecode);
    return bc;
}

int
FunctionBuilder::get_constant_index(Value v) {
    Value v_index;
    if (constant_index_table_->find(v, v_index)) {
        return v_index.get_int();
    }

    int index = constant_table_->get_length();
    constant_table_->append(v);
    constant_index_table_->insert(v, Value::by_int(index));
    return index;
}

void
FunctionBuilder::set_parameter_dispatch_type_list(Array* array) {
    disp_kinds_ = get_current_state()->allocate_block<FunctionInfo::ArgDispatchKind>(array->get_length());
    for (int i = 0; i < array->get_length(); i++) {
        Symbol* s = array->elt_ref(i).get_obj<Symbol>();
        if (s->equals("class_match")) {
            disp_kinds_[i] = FunctionInfo::ArgDispatchKind::Class;
        } else if (s->equals("instance_match")) {
            disp_kinds_[i] = FunctionInfo::ArgDispatchKind::Instance;
        } else {
            throw std::runtime_error("Invalid dispatch kind");
        }
    }
}

void
FunctionBuilder::set_parameter_class_list(Array* array){
    argument_types_ = get_current_state()->allocate_block<Symbol*>(array->get_length());
    for (int i = 0; i < array->get_length(); i++) {
        argument_types_[i] = array->elt_ref(i).get_obj<Symbol>();
    }
}

bool
FunctionBuilder::resolve_labels() {
    for (int i = 0; i < insn_array_->get_length(); i++) {
        uint32_t insn = insn_array_->elt_ref(i);
        switch (insn & 0xff) {
            case Insn::Jump:
            case Insn::IfJump:
            case Insn::UnlessJump: {
                int label_id = get_insn_arg_u(insn);
                int dest = label_table_->find(label_id);
                insn_array_->elt_set(i, encode_insn_arg1(insn & 0xff, dest));
                break;
            }
            default:
                break;
        }
    }

    return true;
}

void
FunctionBuilder::get_required_stack_size_aux(int *st, int c, int h) {
  if (st[c] == h) {
    return;
  } else if (st[c] >= 0 && st[c] != h) {
    throw std::runtime_error("Stack height mismatch");
  } else {
    st[c] = h;
    uint32_t insn = insn_array_->elt_ref(c);
    switch (insn & 0xff) {
      case Insn::Jump:
        get_required_stack_size_aux(st, get_insn_arg_u(insn), h);
        break;
      case Insn::IfJump:
      case Insn::UnlessJump:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, get_insn_arg_u(insn), h - 1);
        get_required_stack_size_aux(st, c + 1, h - 1);
        break;
      case Insn::BinaryOp:
        if (!(h >= 2)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - 1);
        break;
      case Insn::UnaryOp:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h);
        break;
      case Insn::Pop:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - 1);
        break;
      case Insn::Ret:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        break;
      case Insn::Call:
      case Insn::Escape: {
        int argc = get_insn_arg_u(insn);
        if (!(h >= argc + 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - argc);
        break;
      }
      case Insn::Ranew:
      case Insn::Mref:
      case Insn::Lfset:
      case Insn::Lvset:
      case Insn::Laset:
      case Insn::Gvset:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h);
        break;
      case Insn::Raref:
        if (!(h >= 2)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h);
        break;
      case Insn::Raset:
        if (!(h >= 3)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - 1);
        break;
      case Insn::Iref:
      case Insn::Mset:
        if (!(h >= 2)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - 1);
        break;
      case Insn::Iset:
        if (!(h >= 3)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h - 2);
        break;
      case Insn::Lfref:
      case Insn::Lvref:
      case Insn::Laref:
      case Insn::Gfref:
      case Insn::Gvref:
      case Insn::Enclose:
      case Insn::Load:
      case Insn::LoadClass:
      case Insn::LoadUndef:
      case Insn::LoadNull:
      case Insn::LoadTrue:
      case Insn::LoadFalse:
        get_required_stack_size_aux(st, c + 1, h + 1);
        break;
      case Insn::Dup:
        if (!(h >= 1)) {
          throw std::runtime_error("Stack underflow");
        }
        get_required_stack_size_aux(st, c + 1, h + 1);
        break;
      default:
        throw std::runtime_error("Invalid op");
        break;
    }
  }
}

unsigned
FunctionBuilder::get_required_stack_size() {
    int *st = new int[insn_array_->get_length()];
    for (int i = 0; i < insn_array_->get_length(); i++) {
        st[i] = -1;
    }
    get_required_stack_size_aux(st, 0, 0);
    int m = 0;
    for (int i = 0; i < insn_array_->get_length(); i++) {
      m = st[i] > m ? st[i] : m;
    }
    return m;
}

bool
FunctionBuilder::slot_ref(Symbol* slot_id, Value& value) {
  if (slot_id->equals("id")) {
    value = Value::by_object(id_);
  } else if (slot_id->equals("name")) {
    value = Value::by_object(name_);
  } else {
    return false;
  }
  return true;
}

void
FunctionBuilder::dump() {
    fprintf(stderr, "------ Begin FunctionBuilder dump ------\n");
    fprintf(stderr, "name: %s\n", name_->get_cstr());
    fprintf(stderr, "id: %s\n", id_->get_cstr());
    fprintf(stderr, "num_args: %d\n", num_args_);
    fprintf(stderr, "variadic: %s\n", variadic_ ? "true" : "false");
    fprintf(stderr, "arguments:\n");
    for (int i = 0; i < num_args_; i++) {
      const char* s = "";
      switch (disp_kinds_[i]) {
          case FunctionInfo::ArgDispatchKind::Class:
              s = "class";
              break;
          case FunctionInfo::ArgDispatchKind::Instance:
              s = "instance";
              break;
          default:
              s = "INVALID";
              break;
      }
      fprintf(stderr, "  [%d] disp_kind: %s\n", i, s);
      fprintf(stderr, "  [%d]     class: %s\n", i, argument_types_[i]->get_cstr());
    }
    fprintf(stderr, "func_slot_size: %d\n", func_slot_size_);
    fprintf(stderr, "var_slot_size: %d\n", var_slot_size_);
    fprintf(stderr, "constant_table:\n");
    for (int i = 0; i < constant_table_->get_length(); i++) {
      Value v = constant_table_->elt_ref(i);
      fprintf(stderr, "  [%d]: %s\n", i, v.get_string_representation()->get_cstr());
    }
    fprintf(stderr, "instructions:\n");
    for (int i = 0; i < insn_array_->get_length(); i++) {
      uint32_t insn = insn_array_->elt_ref(i);
      switch (insn & 0xff) {
        case Insn::Jump:
          fprintf(stderr, "  [%d]: jump %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::IfJump:
          fprintf(stderr, "  [%d]: if_jump %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::UnlessJump:
          fprintf(stderr, "  [%d]: unless_jump %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::BinaryOp:
          fprintf(stderr, "  [%d]: binary_op %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::UnaryOp:
          fprintf(stderr, "  [%d]: unary_op %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Pop:
          fprintf(stderr, "  [%d]: pop\n", i);
          break;
        case Insn::Ret:
          fprintf(stderr, "  [%d]: ret\n", i);
          break;
        case Insn::Call:
          fprintf(stderr, "  [%d]: call %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Escape:
          fprintf(stderr, "  [%d]: escape %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Ranew:
          fprintf(stderr, "  [%d]: ranew\n", i);
          break;
        case Insn::Mref:
          fprintf(stderr, "  [%d]: mref %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Lfset:
          fprintf(stderr, "  [%d]: lfset %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Lvset:
          fprintf(stderr, "  [%d]: lvset %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Laset:
          fprintf(stderr, "  [%d]: laset %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Gvset:
          fprintf(stderr, "  [%d]: gvset %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Raref:
          fprintf(stderr, "  [%d]: raref\n", i);
          break;
        case Insn::Raset:
          fprintf(stderr, "  [%d]: raset\n", i);
          break;
        case Insn::Iref:
          fprintf(stderr, "  [%d]: iref\n", i);
          break;
        case Insn::Mset:
          fprintf(stderr, "  [%d]: mset %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Iset:
          fprintf(stderr, "  [%d]: iset\n", i);
          break;
        case Insn::Lfref:
          fprintf(stderr, "  [%d]: lfref %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Lvref:
          fprintf(stderr, "  [%d]: lvref %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Laref:
          fprintf(stderr, "  [%d]: laref %d, %d\n", i, get_insn_arg_uu1(insn), get_insn_arg_uu2(insn));
          break;
        case Insn::Gfref:
          fprintf(stderr, "  [%d]: gfref %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Gvref:
          fprintf(stderr, "  [%d]: gvref %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Enclose:
          fprintf(stderr, "  [%d]: enclose %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::Load:
          fprintf(stderr, "  [%d]: load %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::LoadClass:
          fprintf(stderr, "  [%d]: load_class %d\n", i, get_insn_arg_u(insn));
          break;
        case Insn::LoadUndef:
          fprintf(stderr, "  [%d]: load_undef\n", i);
          break;
        case Insn::LoadNull:
          fprintf(stderr, "  [%d]: load_null\n", i);
          break;
        case Insn::LoadTrue:
          fprintf(stderr, "  [%d]: load_true\n", i);
          break;
        case Insn::LoadFalse:
          fprintf(stderr, "  [%d]: load_false\n", i);
          break;
        case Insn::Dup:
          fprintf(stderr, "  [%d]: dup\n", i);
          break;
        default:
          fprintf(stderr, "  [%d]: UNKNOWN OP\n", i);
          break;
      }
    }
    fprintf(stderr, "------ End FunctionBuilder dump ------\n");
}

bool
FunctionBuilder::store(FILE* fp) {
  binary::writeByte(fp, ObjectSigniture::Function);
  binary::writeSymbol(fp, id_);
  binary::writeByte(fp, variadic_ ? 1 : 0);
  binary::writeBER(fp, num_args_);
  for (int i = 0; i < num_args_; i++) {
    char d;
    switch (disp_kinds_[i]) {
      case FunctionInfo::ArgDispatchKind::Instance:
        d = 1;
        break;
      case FunctionInfo::ArgDispatchKind::Class:
        d = 0;
        break;
      default:
        break;
    }
    binary::writeByte(fp, d);
    binary::writeSymbol(fp, argument_types_[i]);
  }
  binary::writeBER(fp, func_slot_size_);
  binary::writeBER(fp, var_slot_size_);
  binary::writeBER(fp, stack_size_);

  binary::writeBER(fp, constant_table_->get_length());
  for (int i = 0; i < constant_table_->get_length(); i++) {
    Value v = constant_table_->elt_ref(i);
    if (v.is(Value::Type::Int)) {
      binary::writeByte(fp, LiteralSigniture::Int);
      binary::writeInt(fp, v.get_int());
    } else if (v.is(Value::Type::Char)) {
      binary::writeByte(fp, LiteralSigniture::Char);
      binary::write32Bit(fp, v.get_char());
    } else if (v.is(Value::Type::Object)) {
      ClassID id = v.get_obj<Object>()->get_class_id();
      if (id == BuiltinClassID::Symbol) {
        binary::writeByte(fp, LiteralSigniture::Symbol);
        binary::writeSymbol(fp, v.get_obj<Symbol>());
      } else if (id == BuiltinClassID::String) {
        binary::writeByte(fp, LiteralSigniture::String);
        binary::writeString(fp, v.get_obj<String>());
      } else {
        throw std::runtime_error("Invalid constant");
      }
    } else {
      throw std::runtime_error("Invalid constant");
    }
  }
  binary::writeBER(fp, insn_array_->get_length());
  for (int i = 0; i < insn_array_->get_length(); i++) {
    binary::write32Bit(fp, insn_array_->elt_ref(i));
  }
  return true;
}

ClassBuilder*
ClassBuilder::create() {
    void *p = get_current_state()->allocate_object<ClassBuilder>();
    return new (p) ClassBuilder();
}

ClassBuilder::ClassBuilder()
    : Object(class_builder_class_id), name_(nullptr), id_(nullptr),
      parent_name_(nullptr), num_slots_(0) {

    slots_ = SysArray<Symbol*>::create();
}

void
ClassBuilder::add_slot(Symbol* slot_name) {
    slots_->append(slot_name);
    ++num_slots_;
}

void
ClassBuilder::dump() {
  fprintf(stderr, "------ Begin ClassBuilder dump ------\n");
  fprintf(stderr, "slots:\n");
  for (int i = 0; i < num_slots_; i++) {
    fprintf(stderr, "  [%d]: %s\n", i, slots_->elt_ref(i)->get_cstr());
  }
  fprintf(stderr, "------ End ClassBuilder dump ------\n");
}

bool
ClassBuilder::store(FILE* fp) {
  binary::writeByte(fp, ObjectSigniture::Class);
  binary::writeSymbol(fp, name_);
  binary::writeSymbol(fp, parent_name_);
  binary::writeBER(fp, slots_->get_length());
  for (int i = 0; i < slots_->get_length(); i++) {
    binary::writeSymbol(fp, slots_->elt_ref(i));
  }
}

ModuleBuilder::ModuleBuilder()
    : Object(module_builder_class_id) {

    functions_ = SysArray<FunctionBuilder*>::create();
    classes_ = SysArray<ClassBuilder*>::create();
    variables_ = SysArray<Symbol*>::create();
}

bool
ModuleBuilder::initialize() {
    return false;
}

ModuleBuilder*
ModuleBuilder::create() {
    void *p = get_current_state()->allocate_object<ModuleBuilder>();
    return new (p) ModuleBuilder();
}

void
ModuleBuilder::register_function(FunctionBuilder* function_builder) {
    functions_->append(function_builder);
}

void
ModuleBuilder::register_class(ClassBuilder* class_builder) {
    classes_->append(class_builder);
}

void
ModuleBuilder::register_variable(Symbol* name) {
    variables_->append(name);
}

void
ModuleBuilder::set_initializer(Symbol* name) {
    initializer_ = name;
}

void
ModuleBuilder::dump() {
    fprintf(stderr, "------ Begin ModuleBuilder dump ------\n");
    fprintf(stderr, "------ Functions ------\n");
    for (int i = 0; i < functions_->get_length(); i++) {
        functions_->elt_ref(i)->dump();
    }
    fprintf(stderr, "------ Classes ------\n");
    for (int i = 0; i < classes_->get_length(); i++) {
        classes_->elt_ref(i)->dump();
    }
    fprintf(stderr, "------ Variables ------\n");
    for (int i = 0; i < variables_->get_length(); i++) {
        classes_->elt_ref(i)->dump();
    }
    fprintf(stderr, "------ End ModuleBuilder dump ------\n");
}

bool
ModuleBuilder::store(FILE* fp) {
  int num_objects = functions_->get_length()
    + classes_->get_length()
    + variables_->get_length();
  binary::writeSymbol(fp, initializer_);
  binary::writeBER(fp, num_objects);
  for (int i = 0; i < classes_->get_length(); i++) {
    classes_->elt_ref(i)->store(fp);
  }
  for (int i = 0; i < functions_->get_length(); i++) {
    functions_->elt_ref(i)->store(fp);
  }
}

BytecodeModule*
BytecodeModule::create() {
    void *p = get_current_state()->allocate_object<ModuleBuilder>();
    return new (p) BytecodeModule();
}

Value
fn_unary_op(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->unary_op(args[1].get_int());
    return Value::k_nil();
}

Value
fn_binary_op(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->binary_op(args[1].get_int());
    return Value::k_nil();
}


Value
fn_jump(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->jump(args[1].get_int());
    return Value::k_nil();
}


Value
fn_if_jump(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->if_jump(args[1].get_int());
    return Value::k_nil();
}


Value
fn_unless_jump(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->unless_jump(args[1].get_int());
    return Value::k_nil();
}


Value
fn_call(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->call(args[1].get_int());
    return Value::k_nil();
}


Value
fn_ret(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->ret();
    return Value::k_nil();
}


Value
fn_ranew(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->ranew();
    return Value::k_nil();
}


Value
fn_raref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->raref();
    return Value::k_nil();
}


Value
fn_raset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->raset();
    return Value::k_nil();
}


Value
fn_iref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->iref();
    return Value::k_nil();
}

Value
fn_iset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->iset();
    return Value::k_nil();
}

Value
fn_mref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->mref(args[1].get_obj<Symbol>());
    return Value::k_nil();
}


Value
fn_mset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->mset(args[1].get_obj<Symbol>());
    return Value::k_nil();
}


Value
fn_lfref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->lfref(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_lfset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->lfset(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_lvref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->lvref(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_lvset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->lvset(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_laset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->laset(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_laref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->laref(args[1].get_int(), args[2].get_int());
    return Value::k_nil();
}

Value
fn_gfref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->gfref(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_gvref(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->gvref(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_gvset(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->gvset(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_enclose(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->enclose(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_dup(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->dup();
    return Value::k_nil();
}


Value
fn_pop(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->pop();
    return Value::k_nil();
}

Value
fn_escape(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->escape(args[1].get_int());
    return Value::k_nil();
}

Value
fn_load_value(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load(args[1]);
    return Value::k_nil();
}

Value
fn_load_klass(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load_klass(args[1].get_obj<Symbol>());
    return Value::k_nil();
}


Value
fn_load_undef(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load_undef();
    return Value::k_nil();
}


Value
fn_load_nil(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load_nil();
    return Value::k_nil();
}

Value
fn_load_true(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load_true();
    return Value::k_nil();
}

Value
fn_load_false(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->load_false();
    return Value::k_nil();
}

Value
fn_label(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->label(args[1].get_int());
    return Value::k_nil();
}

Value
fn_generate_label_id(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    return Value::by_int(b->generate_label_id());
}

Value
fn_make_function_builder(unsigned, Value*) {
    return Value::by_object(FunctionBuilder::create());
}

Value
fn_fb_set_name(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_name(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_fb_set_id(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_id(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_fb_set_variadic(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_variadic(args[1].get_bool());
    return Value::k_nil();
}

Value
fn_fb_set_num_args(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_num_args(args[1].get_int());
    return Value::k_nil();
}

Value
fn_fb_set_arg_type(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_arg_type(args[1].get_int(), args[2].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_fb_set_num_func_slots(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_num_func_slots(args[1].get_int());
    return Value::k_nil();
}

Value
fn_fb_set_num_var_slots(unsigned, Value* args) {
    FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
    b->set_num_var_slots(args[1].get_int());
    return Value::k_nil();
}

Value
fn_fb_set_parameter_dispatch_type_list(unsigned, Value* args) {
  FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
  b->set_parameter_dispatch_type_list(args[1].get_obj<Array>());
  return Value::k_nil();
}

Value
fn_fb_set_parameter_class_list(unsigned, Value* args) {
  FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
  b->set_parameter_class_list(args[1].get_obj<Array>());
  return Value::k_nil();
}

Value
fn_fb_build_function(unsigned, Value* args) {
  FunctionBuilder* b = args[0].get_obj<FunctionBuilder>();
  b->build_function();
  return Value::k_nil();
}

Value
fn_make_class_builder(unsigned, Value*) {
    return Value::by_object(ClassBuilder::create());
}

Value
fn_cb_set_name(unsigned, Value* args) {
    ClassBuilder* c = args[0].get_obj<ClassBuilder>();
    c->set_name(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_cb_set_id(unsigned, Value* args) {
    ClassBuilder* c = args[0].get_obj<ClassBuilder>();
    c->set_id(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_cb_set_parent_name(unsigned, Value* args) {
    ClassBuilder* c = args[0].get_obj<ClassBuilder>();
    c->set_parent_name(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_cb_add_slot(unsigned, Value* args) {
    ClassBuilder* c = args[0].get_obj<ClassBuilder>();
    c->add_slot(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_make_module_builder(unsigned, Value*) {
    return Value::by_object(ModuleBuilder::create());
}

Value
fn_mb_register_function(unsigned, Value* args) {
    ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
    mb->register_function(args[1].get_obj<FunctionBuilder>());
    return Value::k_nil();
}

Value
fn_mb_register_class(unsigned, Value* args) {
    ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
    mb->register_class(args[1].get_obj<ClassBuilder>());
    return Value::k_nil();
}

Value
fn_mb_register_variable(unsigned, Value* args) {
    ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
    mb->register_variable(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_mb_set_initializer(unsigned, Value* args) {
    ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
    mb->set_initializer(args[1].get_obj<Symbol>());
    return Value::k_nil();
}

Value
fn_mb_dump(unsigned, Value* args) {
    ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
    mb->dump();
    return Value::k_nil();
}

Value
fn_mb_store(unsigned, Value* args) {
  ModuleBuilder* mb = args[0].get_obj<ModuleBuilder>();
  FILE* fp = fopen(args[1].get_obj<String>()->get_cstr(), "w");
  if (fp == NULL) {
    return Value::by_bool(false);
  }

  mb->store(fp);
  fclose(fp);

  return Value::by_bool(true);
}

bool
BytecodeModule::initialize() {
    function_builder_class_id = add_class("FunctionBuilder", "Any");
    class_builder_class_id = add_class("ClassBuilder", "Any");
    module_builder_class_id = add_class("ModuleBuilder", "Any");

    add_native_function("make_function_builder", false, {}, fn_make_function_builder);
    add_native_function("set_name", false, {"FunctionBuilder", "Symbol"}, fn_fb_set_name);
    add_native_function("set_id", false, {"FunctionBuilder", "Symbol"}, fn_fb_set_id);
    add_native_function("set_variadic", false, {"FunctionBuilder", "Bool"}, fn_fb_set_variadic);
    add_native_function("set_num_args", false, {"FunctionBuilder", "Int"}, fn_fb_set_num_args);
    add_native_function("set_arg_type", false, {"FunctionBuilder", "Int", "Symbol"}, fn_fb_set_arg_type);
    add_native_function("set_num_func_slots", false, {"FunctionBuilder", "Int"}, fn_fb_set_num_func_slots);
    add_native_function("set_num_var_slots", false, {"FunctionBuilder", "Int"}, fn_fb_set_num_var_slots);
    add_native_function("set_parameter_dispatch_type_list", false, {"FunctionBuilder", "Array"}, fn_fb_set_parameter_dispatch_type_list);
    add_native_function("set_parameter_class_list", false, {"FunctionBuilder", "Array"}, fn_fb_set_parameter_class_list);

    add_native_function("unary_op", false, {"FunctionBuilder", "Int"}, fn_unary_op);
    add_native_function("binary_op", false, {"FunctionBuilder", "Int"}, fn_binary_op);
    add_native_function("jump", false, {"FunctionBuilder", "Int"}, fn_jump);
    add_native_function("if_jump", false, {"FunctionBuilder", "Int"}, fn_if_jump);
    add_native_function("unless_jump", false, {"FunctionBuilder", "Int"}, fn_unless_jump);
    add_native_function("call", false, {"FunctionBuilder", "Int"}, fn_call);
    add_native_function("ret", false, {"FunctionBuilder"}, fn_ret);
    add_native_function("ranew", false, {"FunctionBuilder"}, fn_ranew);
    add_native_function("raref", false, {"FunctionBuilder"}, fn_raref);
    add_native_function("raset", false, {"FunctionBuilder"}, fn_raset);
    add_native_function("iref", false, {"FunctionBuilder"}, fn_iref);
    add_native_function("iset", false, {"FunctionBuilder"}, fn_iset);
    add_native_function("mref", false, {"FunctionBuilder", "Symbol"}, fn_mref);
    add_native_function("mset", false, {"FunctionBuilder", "Symbol"}, fn_mset);
    add_native_function("lfref", false, {"FunctionBuilder", "Int", "Int"}, fn_lfref);
    add_native_function("lfset", false, {"FunctionBuilder", "Int", "Int"}, fn_lfset);
    add_native_function("lvref", false, {"FunctionBuilder", "Int", "Int"}, fn_lvref);
    add_native_function("lvset", false, {"FunctionBuilder", "Int", "Int"}, fn_lvset);
    add_native_function("laset", false, {"FunctionBuilder", "Int", "Int"}, fn_laset);
    add_native_function("laref", false, {"FunctionBuilder", "Int", "Int"}, fn_laref);
    add_native_function("gfref", false, {"FunctionBuilder", "Symbol"}, fn_gfref);
    add_native_function("gvref", false, {"FunctionBuilder", "Symbol"}, fn_gvref);
    add_native_function("gvset", false, {"FunctionBuilder", "Symbol"}, fn_gvset);
    add_native_function("enclose", false, {"FunctionBuilder", "Symbol"}, fn_enclose);
    add_native_function("dup", false, {"FunctionBuilder"}, fn_dup);
    add_native_function("pop", false, {"FunctionBuilder"}, fn_pop);
    add_native_function("escape", false, {"FunctionBuilder", "Int"}, fn_escape);
    add_native_function("load_value", false, {"FunctionBuilder", "Any"}, fn_load_value);
    add_native_function("load_class", false, {"FunctionBuilder", "Symbol"}, fn_load_klass);
    add_native_function("load_undef", false, {"FunctionBuilder"}, fn_load_undef);
    add_native_function("load_nil", false, {"FunctionBuilder"}, fn_load_nil);
    add_native_function("load_true", false, {"FunctionBuilder"}, fn_load_true);
    add_native_function("load_false", false, {"FunctionBuilder"}, fn_load_false);
    add_native_function("label", false, {"FunctionBuilder", "Int"}, fn_label);
    add_native_function("generate_label_id", false, {"FunctionBuilder"}, fn_generate_label_id);
    add_native_function("build_function", false, {"FunctionBuilder"}, fn_fb_build_function);

    add_native_function("make_class_builder", false, {}, fn_make_class_builder);
    add_native_function("set_name", false, {"ClassBuilder", "Symbol"}, fn_cb_set_name);
    add_native_function("set_id", false, {"ClassBuilder", "Symbol"}, fn_cb_set_id);
    add_native_function("set_parent_name", false, {"ClassBuilder", "Symbol"}, fn_cb_set_parent_name);
    add_native_function("add_slot", false, {"ClassBuilder", "Symbol"}, fn_cb_add_slot);

    add_native_function("make_module_builder", false, {}, fn_make_module_builder);
    add_native_function("register_function", false, {"ModuleBuilder", "FunctionBuilder"}, fn_mb_register_function);
    add_native_function("register_class", false, {"ModuleBuilder", "ClassBuilder"}, fn_mb_register_class);
    add_native_function("register_variable", false, {"ModuleBuilder", "Symbol"}, fn_mb_register_variable);
    add_native_function("set_initializer", false, {"ModuleBuilder", "Symbol"}, fn_mb_set_initializer);
    add_native_function("dump", false, {"ModuleBuilder"}, fn_mb_dump);
    add_native_function("store", false, {"ModuleBuilder", "String"}, fn_mb_store);
    return false;
}

}
