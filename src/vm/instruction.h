//
// instruction.h - Opcode definition
//

namespace rhein {

struct Insn {
    enum InstructionCode {
        UnaryOp,    // 0
        BinaryOp,
        Jump,
        IfJump,
        UnlessJump,
        Call,
        Ret,
        Ranew,
        Raref,
        Raset,
        Iref,       // 10
        Iset,
        Mref,
        Mset,
        Lfref,
        Lfset,
        Lvref,
        Lvset,
        Laref,
        Laset,
        Gfref,      // 20
        Gvref,
        Gvset,
        Load,
        LoadClass,
        LoadUndef,
        LoadNull,
        LoadTrue,
        LoadFalse,
        Enclose,
        Dup,        // 30
        Pop,
        Escape,
        Break,      // 33
    };
};

inline uint32_t
get_insn_arg_u(uint32_t insn) {
    return insn >> 8;
}

inline uint32_t
get_insn_arg_uu1(uint32_t insn) {
    return (insn >> 8) & 0xff;
}

inline uint32_t
get_insn_arg_uu2(uint32_t insn) {
    return (insn >> 16) & 0xff;
}

inline uint32_t
encode_insn_arg0(uint8_t opcode) {
    return opcode;
}

inline uint32_t
encode_insn_arg1(uint8_t opcode, int arg) {
    return (arg << 8) | opcode;
}

inline uint32_t
encode_insn_arg2(uint8_t opcode, int arg1, int arg2) {
    return (arg1 << 8) | (arg2 << 16) | opcode;
}

}
