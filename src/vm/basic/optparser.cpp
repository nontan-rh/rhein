
#include <cstring>

#include "object.h"

#include "basic/optparser.h"

namespace rhein {
namespace optparser {

ClassID optparser_class_id;

OptParser*
OptParser::create() {
  return new (get_current_state()->allocate_object<OptParser>()) OptParser();
}

OptParser::OptParser()
  : Object(optparser_class_id) {
  for (int i = 0; i < 256; i++) {
    flags_[i] = false;
  }
}

bool
OptParser::get_flag(char ch) const {
  return flags_[ch];
}

void
OptParser::parse(int argc, const char** argv) {
  int cnt = 0;
  for (int i = 0; i < argc; i++) {
    int len = std::strlen(argv[i]);
    if (len > 0 && argv[i][0] == '-') {
      for (int j = 1; j < len; j++) {
        flags_[argv[i][j]] = true;
      }
    } else {
      ++cnt;
    }
  }
  argc_ = cnt;

  args_c_ = new const char*[argc_];
  cnt = 0;
  for (int i = 0; i < argc; i++) {
    int len = std::strlen(argv[i]);
    if (len > 0 && argv[i][0] != '-') {
      args_c_[cnt] = argv[i];
      ++cnt;
    }
  }

  args_ = Array::create(argc_);
  for (int i = 0; i < argc_; i++) {
    args_->elt_set(i, Value::by_object(String::create(args_c_[i])));
  }
}

Array*
OptParser::get_args() const {
  return args_;
}

const char**
OptParser::get_args_c(int& argc) const {
  argc = argc_;
  return args_c_;
}

}
}
