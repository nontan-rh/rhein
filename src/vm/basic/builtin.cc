//
// builtin.cc
//

#include <stdexcept>

#include "vm.h"
#include "error.h"
#include "object.h"
#include "instruction.h"

#include "basic/builtin.h"

namespace rhein {
namespace builtin {

static Value
register_function(unsigned /* argc */, Value* args) {
    State* R = get_current_state();
    Value fn = args[0];
    Value name = args[1];

    R->add_function(name.get_obj<Symbol>(), fn.get_obj<Function>());
    return Value::k_nil();
}

static Value
register_variable(unsigned /* argc */, Value* args) {
    State* R = get_current_state();
    R->add_variable(args[0].get_obj<Symbol>(), args[1]);
    return Value::k_nil();
}

static uint32_t
encode_instruction(Array* insn) {
    int length = insn->get_length();
    if (length == 1) {
        return encode_insn_arg0(insn->elt_ref(0).get_int());
    } else if (length == 2) {
        return encode_insn_arg1(insn->elt_ref(0).get_int(),
                                insn->elt_ref(1).get_int());
    } else if (length == 3) {
        return encode_insn_arg2(insn->elt_ref(0).get_int(),
                                insn->elt_ref(1).get_int(),
                                insn->elt_ref(2).get_int());
    } else {
        throw std::logic_error("");
    }
}

static Value
build_function_aux(unsigned, Value* args) {
    State* R = get_current_state();

    // Symbol* name = args[0].get_obj<Symbol>();
    Symbol* id = args[1].get_obj<Symbol>();
    bool is_variadic = args[2].get_bool();
    Array* parameter_class_list = args[3].get_obj<Array>();
    Array* parameter_dispatch_type_list = args[4].get_obj<Array>();
    Array* instructions = args[5].get_obj<Array>();
    Array* constant_table = args[6].get_obj<Array>();
    Int required_stack_size = args[7].get_int();
    Int num_func_slots = args[8].get_int();
    Int num_var_slots = args[9].get_int();

    unsigned constant_table_size = constant_table->get_length();
    Value* raw_constant_table = R->allocate_raw_array(constant_table_size);
    for (unsigned i = 0; i < constant_table_size; i++) {
        raw_constant_table[i] = constant_table->elt_ref(i);
    }

    unsigned bytecode_size = instructions->get_length();
    uint32_t* bytecode = R->allocate_block<uint32_t>(bytecode_size);
    for (unsigned i = 0; i < bytecode_size; i++) {
        bytecode[i] = encode_instruction(instructions->elt_ref(i).get_obj<Array>());
    }

    unsigned num_params = parameter_class_list->get_length();
    FunctionInfo::ArgDispatchKind* disp_types = R->allocate_block<FunctionInfo::ArgDispatchKind>(num_params);
    ClassID* param_class_ids = R->allocate_block<ClassID>(num_params);
    for (unsigned i = 0; i < num_params; i++) {
        disp_types[i] = static_cast<FunctionInfo::ArgDispatchKind>(parameter_dispatch_type_list->elt_ref(i).get_int());
        param_class_ids[i] = parameter_class_list->elt_ref(i).get_class();
    }

    BytecodeFunction* bc = BytecodeFunction::create(
            FunctionInfo::create(
                id,
                is_variadic,
                num_params,
                disp_types,
                param_class_ids),
            num_func_slots,
            num_var_slots,
            required_stack_size,
            constant_table_size,
            raw_constant_table,
            bytecode_size,
            bytecode);
    return Value::by_object(bc);
}

static Value
dispatch_class_aux(unsigned, Value* args) {
    State* R = get_current_state();

    Symbol* name = args[0].get_obj<Symbol>();
    // Symbol* id = args[1].get_obj<Symbol>();
    Symbol* parent_name = args[2].get_obj<Symbol>();
    Array* slot_name_list = args[3].get_obj<Array>();

    ClassID parent_id;
    if (!R->get_class(parent_name, parent_id)) {
        throw std::runtime_error("no such class");
    }

    Int num_slots = slot_name_list->get_length();
    Symbol** raw_slot_name_list = R->allocate_block<Symbol*>(num_slots);
    for (Int i = 0; i < num_slots; i++) {
        raw_slot_name_list[i] = slot_name_list->elt_ref(i).get_obj<Symbol>();
    }

    RecordInfo* parent_record_info;
    if (!R->get_class_record_info(parent_id, parent_record_info)) {
        throw std::runtime_error("no such class");
    }

    ClassID result = R->add_class(
            name,
            parent_id,
            RecordInfo::create(
                parent_record_info,
                num_slots,
                raw_slot_name_list));

    return Value::by_class(result);
}

BuiltinModule*
BuiltinModule::create() {
    State* R = get_current_state();
    void* p = R->allocate_struct<BuiltinModule>();
    return new (p) BuiltinModule;
}

bool
BuiltinModule::initialize() {
    add_native_function("!!register_function", false, {"Any", "Symbol"},
        register_function);
    add_native_function("!!register_variable", false, {"Symbol", "Any"},
        register_variable);
    add_native_function("build_function_aux",
                        false,
                        {"Symbol", "Symbol", "Bool", "Array", "Array",
                         "Array", "Array", "Int", "Int", "Int"},
                        build_function_aux);
    add_native_function("dispatch_class_aux",
                        false,
                        {"Symbol", "Symbol", "Symbol", "Array"},
                        dispatch_class_aux);
    add_native_function("register_function", false, {"Any", "Symbol"},
                        register_function);
    add_native_function("register_variable", false, {"Symbol", "Any"},
                        register_variable);
    return false;
}

}

}

