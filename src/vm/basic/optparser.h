
#ifndef OPTPARSER_H_INCLUDED
#define OPTPARSER_H_INCLUDED

#include <cstdint>

#include "object.h"
#include "vm.h"

namespace rhein {
namespace optparser {

extern ClassID optparser_class_id;

class OptParser : public Object {
public:
  static OptParser* create();

  // Interface for c++
  bool get_flag(char ch) const;
  void parse(int argc, const char** argv);

  // Interface for rhein
  Array* get_args() const;
  const char** get_args_c(int& argc) const;

private:
  int argc_;
  bool flags_[256];

  const char** args_c_;
  Array* args_;

  OptParser();
};

}
}

#endif // OPTPARSER_H_INCLUDED
