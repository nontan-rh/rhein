//
// mtrandom.h
//

#ifndef MTRANDOM_H_INCLUDED
#define MTRANDOM_H_INCLUDED

#include <cstdint>

#include "object.h"
#include "vm.h"

namespace rhein {
namespace mtrandom {

#define N 624

class MTRandom : public Object {
public:
    static MTRandom* create(uint32_t s);

    /* generates a random number on [0,0xffffffff]-interval */
    uint32_t genrand_int32();
    /* generates a random number on [0,0x7fffffff]-interval */
    int32_t genrand_int31();
    /* generates a random number on [0,1]-real-interval */
    double genrand_real1();
    /* generates a random number on [0,1)-real-interval */
    double genrand_real2();
    /* generates a random number on (0,1)-real-interval */
    double genrand_real3();
    /* generates a random number on [0,1) with 53-bit resolution */
    double genrand_res53();

private:
    uint32_t mt[N]; /* the array for the state vector  */
    int mti;
    uint32_t mag01[2];

    /* initializes with a seed */
    MTRandom(uint32_t s);
    /* initialize by an array with array-length */
    MTRandom(uint32_t init_key[], int key_length);

    void init_genrand(uint32_t s);
};

class MTModule : public Module, public PlacementNewObj {
public:
    static MTModule* create();
    bool initialize();
};

}
}

#endif // For include guard

