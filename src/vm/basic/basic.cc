//
// basic.cc
//

#include <cstdio>
#include <cstring>

#include "object.h"
#include "vm.h"
#include "loader.h"
#include "basic/basic.h"
#include "basic/optparser.h"
#include "error.h"

namespace rhein {
namespace basic {

ClassID list_class_id;
SingleList* end_of_list;

void
print_value(Value v) {
    if (v.is(Value::Type::Int)) {
        printf("%d", v.get_int());
    } else if (v.is(Value::Type::Char)) {
        printf("?%c(%d)", v.get_char(), v.get_char());
    } else if (v.is(Value::Type::Bool)) {
        if (v.get_bool()) {
            printf("true");
        } else {
            printf("false");
        }
    } else if (v.is(Value::Type::Nil)) {
        printf("nil");
    } else if (v.is(Value::Type::Object)) {
        String* str = v.get_obj<Object>()->get_string_representation();
        const char* cstr;
        size_t len;
        str->get_cstr(cstr, len);
        for (unsigned i = 0; i < len; i++) {
            printf("%c", cstr[i]);
        }
    } else if (v.is(Value::Type::Record)) {
        Record* rec = v.get_rec();
        Symbol* name;
        get_current_state()->get_class_name(get_class_of(rec), name);
        const char* cstr;
        size_t len;
        name->get_cstr(cstr, len);
        printf("#r<");
        for (unsigned i = 0; i < len; i++) {
            printf("%c", cstr[i]);
        }
        printf(">");
    } else if (v.is(Value::Type::Class)) {
        Symbol* name;
        const char* cstr;
        size_t len;
        get_current_state()->get_class_name(v.get_class(), name);
        name->get_cstr(cstr, len);
        printf("#c<");
        for (unsigned i = 0; i < len; i++) {
            printf("%c", cstr[i]);
        }
        printf(">");
    } else if (v.is(Value::Type::Undef)) {
        printf("Undef");
    } else {
        fatal("Cannot print");
    }
}

Value
fn_print(unsigned argc, Value* args) {
    if (argc >= 1) {
        print_value(args[0]);
        for (unsigned i = 1; i < argc; i++) {
            printf(" ");
            print_value(args[i]);
        }
    }
    printf("\n");
    return Value::k_nil();
}

Value
fn_write(unsigned /* argc */, Value* args) {
    Value v = args[0];
    if (v.is(Value::Type::Int)) {
        printf("%d", v.get_int());
    } else if (v.is(Value::Type::Char)) {
        printf("%c", v.get_char());
    } else if (v.is(Value::Type::Bool)) {
        if (v.get_bool()) {
            printf("true");
        } else {
            printf("false");
        }
    } else if (v.is(Value::Type::Nil)) {
        printf("nil");
    } else if (v.is(Value::Type::Object)) {
        String* str = v.get_obj<Object>()->get_string_representation();
        const char* cstr;
        size_t len;
        str->get_cstr(cstr, len);
        for (unsigned i = 0; i < len; i++) {
            printf("%c", cstr[i]);
        }
    } else if (v.is(Value::Type::Record)) {
        printf("#r");
    } else {
        fatal("Cannot print");
    }
    return Value::k_nil();
}

Value
fn_input_0(unsigned /* argc */, Value* /* args */) {
    char buf[256];
    scanf("%255s", buf);

    return Value::by_object(String::create(buf));
}

Value
fn_input_1(unsigned /* argc */, Value* args) {
    print_value(args[0]);

    char buf[256];
    scanf("%255s", buf);

    return Value::by_object(String::create(buf));
}

Value
fn_new(unsigned /* argc */, Value* args) {
    return Value::by_record(Record::create(args[0].get_class()));
}

Value
fn_literal(unsigned argc, Value* args) {
    if (argc == 0 || get_class_of(args[0]) != BuiltinClassID::Class) {
        fatal("Class required");
    }

    ClassID k = args[0].get_class();
    if (k == BuiltinClassID::Array) {
        if (!(argc == 2 && get_class_of(args[1]) == BuiltinClassID::Array)) {
            fatal("Lack of argument");
        }

        return Value::by_object(Array::literal(args[1].get_obj<Array>()));
    } else if (k == BuiltinClassID::HashTable) {
        if (!(argc == 3
            && get_class_of(args[1]) == BuiltinClassID::Array
            && get_class_of(args[2]) == BuiltinClassID::Array)) {

            fatal("Lack of argument");
        }

        return Value::by_object(HashTable::literal(
            args[1].get_obj<Array>(),
            args[2].get_obj<Array>()));
    }
    fatal("Not supported class");
    return Value::k_nil();
}

Value
fn_to_array(unsigned /* argc */, Value* args) {
    Array* array;
    if (!args[0].get_obj<String>()->to_array(array)) {
        fatal("Error occurred");
    }

    return Value::by_object(array);
}

Value
fn_to_string(unsigned /* argc */, Value* args) {
    String* string;
    if (!args[0].get_obj<Array>()->to_string(string)) {
        fatal("Error occurred");
    }

    return Value::by_object(string);
}

Value
fn_to_symbol(unsigned, Value* args) {
    String* s = args[0].get_obj<String>();
    return Value::by_object(s->to_symbol());
}

Value
fn_list_to_string(unsigned /* argc */, Value* args) {
    size_t len;
    List* list = args[0].get_obj<List>();

    List* p = list;
    for (len = 0; p != end_of_list; p = p->get_tail().get_obj<List>()) {
        if (!p->get_head().is(Value::Type::Char)) { return Value::k_nil(); }
        len++;
    }
    char* buf = new char[len + 1];

    List* q = list;
    for (int i = 0; q != end_of_list; i++, q = q->get_tail().get_obj<List>()) {
        buf[i] = q->get_head().get_char();
    }
    buf[len] = '\0';

    Value s = Value::by_object(String::create(buf, len));
    delete[] buf;
    return s;
}

Value
fn_nil_to_string(unsigned /* argc */, Value* /* args */) {
    return Value::by_object(String::create(""));
}

Value
fn_append(unsigned argc, Value* args) {
    String* result = args[0].get_obj<String>();
    for (unsigned i = 1; i < argc; i++) {
        if (get_class_of(args[i]) != BuiltinClassID::String) {
            fatal("Cannot append");
        }

        result = result->append(args[i].get_obj<String>());
    }
    return Value::by_object(result);
}

Value
fn_array_append(unsigned, Value* args) {
    args[0].get_obj<Array>()->append(args[1]);
    return args[0];
}

Value
fn_head(unsigned, Value* args) {
    return Value::by_object(args[0].get_obj<String>()->head(args[1].get_int()));
}

Value
fn_tail(unsigned, Value* args) {
    return Value::by_object(args[0].get_obj<String>()->tail(args[1].get_int()));
}

Value
fn_sub(unsigned, Value* args) {
    return Value::by_object(args[0].get_obj<String>()->sub(args[1].get_int(), args[2].get_int()));
}

Value
fn_length(unsigned argc, Value* args) {
    if (argc != 1) {
        fatal("Invalid arguments");
    }

    if (get_class_of(args[0]) == BuiltinClassID::Symbol) {
        return Value::by_int(args[0].get_obj<Symbol>()->get_length());
    } else if (get_class_of(args[0]) == BuiltinClassID::String) {
        return Value::by_int(args[0].get_obj<String>()->get_length());
    } else if (get_class_of(args[0]) == BuiltinClassID::Array) {
        return Value::by_int(args[0].get_obj<Array>()->get_length());
    } else if (get_class_of(args[0]) == BuiltinClassID::RestArguments) {
        return Value::by_int(args[0].get_obj<RestArguments>()->get_length());
    } else {
        fatal("Cannot get length");
    }
    return Value::k_nil();
}

Value
fn_die(unsigned argc, Value* args) {
    if (argc >= 1) {
        fn_print(argc, args);
        fflush(stdout);
    }
    exit(1);
    // NOTREACHED
}

Value
fn_is_a(unsigned /* argc */, Value* args) {
    ClassID objklass = get_class_of(args[0]);
    ClassID cmpklass = args[1].get_class();
    return Value::by_bool(is_subclass_of(objklass, cmpklass));
}

Value
fn_load(unsigned /* argc */, Value* args) {
    char *fn;
    const char *buf;
    size_t len;
    args[0].get_obj<String>()->get_cstr(buf, len);
    fn = (char*)malloc(sizeof(char) * (len + 1));
    memcpy(fn, buf, len);
    fn[len] = '\0';

    load_script(fn);

    free(fn);

    return Value::k_true();
}

Value
fn_callback(unsigned, Value* args) {
    return execute(args[0].get_obj<BytecodeFunction>(), 0, nullptr);
}

Value
fn_cons(unsigned, Value* args) {
    SingleList* tail;

    if (args[1].is(Value::Type::Nil)) {
        tail = end_of_list;
    } else if (get_class_of(args[1]) == list_class_id) {
        tail = args[1].get_obj<SingleList>();
    } else {
        return Value::k_nil();
    }

    return Value::by_object(SingleList::create(args[0], tail));
}

Value
fn_to_list(unsigned, Value* args) {
    String* s = args[0].get_obj<String>();
    SingleList* x = end_of_list;

    if (s->get_length() == 0) { return Value::by_object(end_of_list); }

    for (int i = s->get_length() - 1; i >= 0; i--) {
        x = SingleList::create(Value::by_char(s->elt_ref(i)), x);
    }

    return Value::by_object(x);
}

Value
fn_rest_to_array(unsigned, Value* args) {
    Array* ary;
    args[0].get_obj<RestArguments>()->to_array(ary);
    return Value::by_object(ary);
}

Value
fn_array_apply(unsigned, Value* args) {
    Value fn = args[0];
    Array* ary = args[1].get_obj<Array>();
    int len = ary->get_length();
    Value* buf = get_current_state()->allocate_raw_array(len);
    for (int i = 0; i < len; i++) {
        buf[i] = ary->elt_ref(i);
    }
    return execute(fn, len, buf);
}

Value
fn_rest_apply(unsigned, Value* args) {
    Value fn = args[0];
    RestArguments* rest = args[1].get_obj<RestArguments>();
    int len = rest->get_length();
    Value* buf = get_current_state()->allocate_raw_array(len);
    for (int i = 0; i < len; i++) {
        buf[i] = rest->elt_ref(i);
    }
    return execute(fn, len, buf);
}

Value
fn_make_array(unsigned, Value* args) {
    return Value::by_object(Array::create(args[0].get_int()));
}

Value
fn_make_array_with_initial_value(unsigned, Value* args) {
    Int size = args[0].get_int();
    Array* a = Array::create(size);
    for (Int i = 0; i < size; i++) {
        a->elt_set(i, args[1]);
    }
    return Value::by_object(a);
}

Value
fn_concatenate(unsigned argc, Value* args) {
    int size = 0;
    for (unsigned i = 0; i < argc; i++) {
        if (get_class_of(args[i]) == BuiltinClassID::Array) {
            size += args[i].get_obj<Array>()->get_length();
        } else if (get_class_of(args[i]) == BuiltinClassID::RestArguments) {
            size += args[i].get_obj<RestArguments>()->get_length();
        } else {
            assert(false);
        }
    }
    Array* r = Array::create(size);

    int cnt = 0;
    for (unsigned i = 0; i < argc; i++) {
        if (get_class_of(args[i]) == BuiltinClassID::Array) {
            Array* a = args[i].get_obj<Array>();
            for (int j = 0; j < a->get_length(); j++) {
                r->elt_set(cnt, a->elt_ref(j));
                cnt++;
            }
        } else if (get_class_of(args[i]) == BuiltinClassID::RestArguments) {
            RestArguments* ra = args[i].get_obj<RestArguments>();
            for (int j = 0; j < ra->get_length(); j++) {
                r->elt_set(cnt, ra->elt_ref(j));
                cnt++;
            }
        }
    }
    return Value::by_object(r);
}

Value
fn_exists(unsigned, Value* args) {
    HashTable* h = args[0].get_obj<HashTable>();
    Value dummy;
    return Value::by_bool(h->find(args[1], dummy));
}

Value
fn_insert(unsigned, Value* args) {
    Array* a = args[0].get_obj<Array>();
    Int i = args[1].get_int();
    Value v = args[2];
    a->insert(i, v);
    return Value::k_nil();
}

Value
fn_dump_object(unsigned, Value* args) {
    if (!args[0].is(Value::Type::Record)) {
        printf("not record\n");
    } else {
        args[0].get_rec()->dump();
    }

    return Value::k_nil();
}

Value
fn_d(unsigned, Value* args) {
    Value v = args[0];
    static_cast<void>(v);
    return Value::k_nil();
}

Value
fn_find_class(unsigned, Value* args) {
    State* R = get_current_state();
    ClassID result;
    if (!R->get_class(args[0].get_obj<Symbol>(), result)) {
        return Value::k_nil();
    }
    return Value::by_class(result);
}

Value
fn_find_function(unsigned, Value* args) {
    State* R = get_current_state();
    Value result;
    if (!R->global_func_ref(args[0].get_obj<Symbol>(), result)) {
        return Value::k_nil();
    }
    return result;
}

Value
fn_drop(unsigned, Value* args) {
    args[0].get_obj<Array>()->drop(args[1].get_int());
    return Value::k_nil();
}

Value
fn_to_number(unsigned, Value* args) {
    String* s = args[0].get_obj<String>();
    const char* cstr;
    size_t length;
    s->get_cstr(cstr, length);
    long result = strtol(cstr, NULL, 10);
    return Value::by_int(result);
}

Value
fn_ord(unsigned, Value* args) {
  uint32_t c = args[0].get_char();
  return Value::by_int(c);
}

Value
fn_chr(unsigned, Value* args) {
  uint32_t i = args[0].get_int();
  return Value::by_char(i);
}

BasicModule*
BasicModule::create() {
    State* R = get_current_state();
    void* p = R->allocate_struct<BasicModule>();
    return new (p) BasicModule;
}

bool
BasicModule::initialize() {
    list_class_id = add_class("List", "Any");
    optparser::optparser_class_id = add_class("OptParser", "Any");

    add_native_function("print", true, {}, fn_print);
    add_native_function("write", false, {"Any"}, fn_write);
    add_native_function("input", false, {}, fn_input_0);
    add_native_function("input", false, {"Any"}, fn_input_1);
    add_native_function("new", false, {"Class"}, fn_new);
    add_native_function("literal", true, {}, fn_literal);
    add_native_function("to_array", false, {"String"}, fn_to_array);
    add_native_function("to_array", false, {"RestArguments"}, fn_rest_to_array);
    add_native_function("to_string", false, {"Array"}, fn_to_string);
    add_native_function("to_string", false, {"List"}, fn_list_to_string);
    add_native_function("to_string", false, {"Nil"}, fn_nil_to_string);
    add_native_function("to_symbol", false, {"String"}, fn_to_symbol);
    add_native_function("append", true, {"String"}, fn_append);
    add_native_function("append", false, {"Array", "Any"}, fn_array_append);
    add_native_function("head", false, {"String", "Int"}, fn_head);
    add_native_function("tail", false, {"String", "Int"}, fn_tail);
    add_native_function("sub", false, {"String", "Int", "Int"}, fn_sub);
    add_native_function("length", false, {"Any"}, fn_length);
    add_native_function("die", true, {}, fn_die);
    add_native_function("is_a", false, {"Any", "Class"}, fn_is_a);
    add_native_function("load", false, {"String"}, fn_load);
    add_native_function("cons", false, {"Any", "Any"}, fn_cons);
    add_native_function("to_list", false, {"String"}, fn_to_list);
    add_native_function("apply", false, {"Any", "Array"}, fn_array_apply);
    add_native_function("apply", false, {"Any", "RestArguments"}, fn_rest_apply);
    add_native_function("callback", false, {"BytecodeFunction"}, fn_callback);
    add_native_function("make_array", false, {"Int"}, fn_make_array);
    add_native_function("make_array", false, {"Int", "Any"}, fn_make_array_with_initial_value);
    add_native_function("concatenate", true, {}, fn_concatenate);
    add_native_function("exists", false, {"HashTable", "Any"}, fn_exists);
    add_native_function("insert", false, {"Array", "Int", "Any"}, fn_insert);
    add_native_function("dump_object", false, {"Any"}, fn_dump_object);
    add_native_function("d", false, {"Any"}, fn_d);
    add_native_function("find_class", false, {"Symbol"}, fn_find_class);
    add_native_function("find_function", false, {"Symbol"}, fn_find_function);
    add_native_function("drop", false, {"Array", "Int"}, fn_drop);
    add_native_function("to_number", false, {"String"}, fn_to_number);
    add_native_function("ord", false, {"Char"}, fn_ord);
    add_native_function("chr", false, {"Int"}, fn_chr);

    end_of_list = SingleList::create(Value::k_undef(), nullptr);

    add_variable("end_of_list", Value::by_object(end_of_list));

    return false;
}

}

}
