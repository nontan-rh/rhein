/*
 * internal.h
 */

#ifndef INTERNAL_H
#define INTERNAL_H

#include <initializer_list>

namespace rhein {

template <typename K, typename V> class SysTable;
template <typename V> class SysArray;

typedef int16_t ClassID;

class Value;
class Object;
class Symbol;
class String;
struct Frame;
class Closure;
class Array;
class State;
class HashTable;
class Record;
class DispatcherNode;

class PlacementNewObj {
protected:
    static void* operator new (size_t /* size */, void* p) { return p; }
};

class FunctionInfo : public PlacementNewObj {
public:
    enum class ArgDispatchKind {
        Class,
        Instance,
    };

    static FunctionInfo* create(Symbol* id, bool variadic,
            unsigned num_args, ArgDispatchKind* disp_kind,
            ClassID* arg_class_ids);

    Symbol* name() const { return name_; }
    bool variadic() const { return variadic_; }
    unsigned num_args() const { return num_args_; }
    ArgDispatchKind* disp_kinds() const { return disp_kinds_; }
    ClassID* arg_class_ids() const { return arg_class_ids_; }

    bool check_type(unsigned argc, Value* args);

private:
    Symbol* name_;
    bool variadic_;
    unsigned num_args_;
    ArgDispatchKind* disp_kinds_;
    ClassID* arg_class_ids_;

    FunctionInfo(Symbol* name, bool variadic, unsigned num_args,
            ArgDispatchKind* disp_kinds, ClassID* arg_class_ids)
        : name_(name), variadic_(variadic), num_args_(num_args),
          disp_kinds_(disp_kinds), arg_class_ids_(arg_class_ids) { }
};

class RecordInfo {
public:
    static RecordInfo* create(RecordInfo* parent, unsigned slot_num,
        Symbol** slot_ids);

    unsigned num_slots() const { return num_slots_; }
    bool get_slot_index(Symbol* slot_id, unsigned& index) const;

private:
    unsigned num_slots_;
    SysTable<const Symbol*, unsigned>* id_index_table_;

    static void* operator new(size_t /* size */, void *p) { return p; }

    RecordInfo(RecordInfo* parent, unsigned slot_num_,
            Symbol** slot_ids);

};

class ClassInfo : public PlacementNewObj {
public:
    static ClassInfo* create(ClassID parent, Symbol* name,
            RecordInfo* record_info);

    ClassID parent() const { return parent_; }
    Symbol* name() const { return name_; }
    RecordInfo* record_info() const { return record_info_; }

private:
    ClassID parent_;
    Symbol* name_;
    RecordInfo* record_info_;

    ClassInfo(ClassID parent, Symbol* name, RecordInfo* record_info)
        : parent_(parent), name_(name), record_info_(record_info) { }
};

inline unsigned long
calc_string_hash(const char* cstr, size_t length) {
    unsigned long hash_value = 0x1f2e3d4c;
    for (size_t i=0; i<length; i++) {
        // xor
        hash_value ^= cstr[i];
        // left rotate by 1
        unsigned long top = (hash_value >> (sizeof(unsigned long) * 8 - 1)) & 1;
        hash_value <<= 1;
        hash_value |= top;
    }
    return hash_value;
}

}

#endif /* INTERNAL_H_ */

